from django.contrib.auth.decorators import login_required
from django.urls import path, include
from dashboard.views import DashboardIndexView
from financeiro.autocomplete import TipoVerbaAutocomplete
from financeiro.views import TipoVerbaList, TipoVerbaCreate, TipoVerbaDetail, TipoVerbaUpdate, TipoVerbaDelete
from users.decorators import register_resource

urlpatterns = [

    path('financeiro/', register_resource(DashboardIndexView.as_view(page_title='Financeiro', header='Financeiro', columns=[3, 3, 3, 3])), name='financeiro-index'),

    path('financeiro/tipo-verbas/', include([
        path('', register_resource(TipoVerbaList), name='tipo-verba-list'),
        path('create/', register_resource(TipoVerbaCreate), name='tipo-verba-create'),
        path('detail/<int:pk>', register_resource(TipoVerbaDetail), name='tipo-verba-detail'),
        path('update/<int:pk>', register_resource(TipoVerbaUpdate), name='tipo-verba-update'),
        path('delete/<int:pk>', register_resource(TipoVerbaDelete), name='tipo-verba-delete'),
        path('autocomplete/', login_required(TipoVerbaAutocomplete.as_view()), name='tipo-verba-autocomplete'),
    ])),





]

