from django.urls import reverse_lazy
from dashboard.views import DashboardListView, DashboardDeleteView, DashboardUpdateView, DashboardCreateView, \
    DashboardDetailView
from financeiro.filters import TipoVerbaFilter
from financeiro.forms import TipoVerbaForm
from financeiro.models import TipoVerba
from financeiro.tables import TipoVerbaTable


class TipoVerbaList(DashboardListView):

    page_title = 'Tipos de Verbas'
    header = 'Tipo de verba list'

    add_button_title = 'New tipo de verba'
    add_button_url = reverse_lazy('tipo-verba-create')

    table_class = TipoVerbaTable
    filter_class = TipoVerbaFilter
    object = TipoVerba

    def get_queryset(self):
        qs = TipoVerba.objects.filter()
        return qs


class TipoVerbaDetail(DashboardDetailView):

    page_title = 'Tipos de Verbas'
    header = 'Tipo de verba detail'
    object = TipoVerba

    rows_based_on_form = TipoVerbaForm

    def get_queryset(self):
        qs = TipoVerba.objects.filter()
        return qs


class TipoVerbaCreate(DashboardCreateView):

    page_title = 'Tipos de Verbas'
    header = 'Add tipo de verba'

    form_class = TipoVerbaForm
    object = TipoVerba

    show_button_save_continue = True
    owner_include = True

    success_message = 'Tipo de verba created successfully.'

    success_redirect = 'tipo-verba-list'


class TipoVerbaUpdate(DashboardUpdateView):

    page_title = 'Tipos de Verbas'
    header = 'Edit tipo de verba'

    form_class = TipoVerbaForm
    object = TipoVerba

    success_message = 'Tipo de verba updated successfully.'
    success_redirect = 'tipo-verba-list'

    def get_queryset(self):
        qs = TipoVerba.objects.filter()
        return qs


class TipoVerbaDelete(DashboardDeleteView):

    success_message = 'Tipo de verba deleted successfully.'
    success_redirect = 'tipo-verba-list'

    header = 'Delete tipo de verba'

    object = TipoVerba
    validate_owner = False

    def get_queryset(self):
        qs = TipoVerba.objects.filter()
        return qs


