import django_filters

from financeiro.models import TipoVerba


class TipoVerbaFilter(django_filters.FilterSet):

    class Meta:
        model = TipoVerba
        fields = {
            'descricao': ['icontains'],
        }
