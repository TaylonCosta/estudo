from dal import autocomplete

from financeiro.models import TipoVerba


class TipoVerbaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):

        qs = TipoVerba.objects.filter()

        if self.q:
            qs = qs.filter(descricao__icontains=self.q)
        return qs
