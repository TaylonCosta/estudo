from dal import autocomplete, forward
from django import forms

from financeiro.models import TipoVerba


class TipoVerbaForm(forms.ModelForm):

    class Meta:
        model = TipoVerba
        fields = '__all__'
        exclude = ('created_by', 'created_on', 'modified_by', 'modified_on')
