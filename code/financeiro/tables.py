from django_tables2 import columns
from dashboard import tables as dashboard_table
from financeiro.models import TipoVerba


class TipoVerbaTable(dashboard_table.Table):

    class Meta(dashboard_table.Table.Meta):
        model = TipoVerba
        fields = ['descricao', 'ativo']
        exclude = ('select_row', )
