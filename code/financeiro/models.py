from django.db import models

from dashboard.models import DashboardBaseModel


class TipoVerba(DashboardBaseModel):

    descricao = models.CharField(max_length=100)
    ativo = models.BooleanField(default=True)

    class Meta:
        ordering = ('descricao', )

    def __str__(self):
        return self.descricao

    @classmethod
    def set_filter_by_str(cls, q):

        filter = {'descricao__iexact': q}

        return filter



