from .celery import app as celery_app

__all__ = ['celery_app']

__version__ = 'v2.8.6'
VERSION = __version__

__staging_code_version__ = 'v2.8.6'
__staging_database_version__ = '2020-02-03'