import notifications.urls
from django.contrib import admin
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import path, include
from django.conf.urls.static import static
from graphene_django.views import GraphQLView


class PrivateGraphQLView(LoginRequiredMixin, GraphQLView):
    pass


urlpatterns = [
    path('admin/', admin.site.urls),
    path('admin/maintenance-mode/', include('maintenance_mode.urls')),
    path('notifications/', include(notifications.urls, namespace='notifications')),
    path('graphql/', PrivateGraphQLView.as_view(graphiql=True)),
    path('explorer/', include('explorer.urls')),
    path('', include('dashboard.urls')),
    path('', include('users.urls')),
    path('', include('configuracao.urls')),
    path('', include('financeiro.urls')),
    path('', include('estudo.urls')),
    path('', include('exemplo.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

try:
    import __dev__
    if settings.DEBUG and settings.USE_SILK and 'silk' in settings.INSTALLED_APPS:
        urlpatterns += [path('silk/', include('silk.urls', namespace='silk'))]
except ImportError:
    pass