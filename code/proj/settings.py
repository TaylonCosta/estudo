import os

import proj

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'jj=7vjnvvglee4%p2gm)4*(ncu)25w93=p&$ufnqd7t=rc=gun'
VERSION = proj.__version__
STAGING_CODE_VERSION = proj.__staging_code_version__
STAGING_DATABASE_VERSION = proj.__staging_database_version__

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = []

LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'
LOGIN_URL = '/auth/login'
LOGOUT_URL = '/auth/logout'
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
AUTH_USER_MODEL = 'users.User'
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

SYSTEM_SHORT_NAME = 'Integrador k12Pub'
SYSTEM_LONG_NAME = 'Integrador k12 Public'

# Application definition
INSTALLED_APPS = [
    'dal',
    'dal_select2',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.postgres',
    'django.contrib.gis',
    'menu',
    'graphene_django',
    'rest_framework',
    'rest_framework_datatables',
    'crispy_forms',
    'django_tables2',
    'django_filters',
    'explorer',
    'maintenance_mode',
    'dashboard',
    'users',
    'notifications',
    'configuracao',
    'financeiro',
    'estudo',
    'exemplo',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'maintenance_mode.middleware.MaintenanceModeMiddleware',
]

ROOT_URLCONF = 'proj.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.request',
                'django.template.context_processors.debug',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'maintenance_mode.context_processors.maintenance_mode',
                'dashboard.context_processors.dashboard_info',
            ],
        },
    },
]

WSGI_APPLICATION = 'proj.wsgi.application'


# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'pt-BR'

TIME_ZONE = 'America/Sao_Paulo'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, './static_files'),
)

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/files/'

CRISPY_TEMPLATE_PACK = 'bootstrap3'

DJANGO_TABLES2_TEMPLATE = 'django_tables2/bootstrap_custom.html'

DJANGO_NOTIFICATIONS_CONFIG = {'USE_JSONFIELD': True}

MAINTENANCE_MODE_IGNORE_SUPERUSER = True
MAINTENANCE_MODE_IGNORE_ADMIN_SITE = True

USE_CELERY = True
USE_SILK = False
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
        'simple': {
            'format': '{levelname} {message}',
            'style': '{',
        },
    },
    'handlers': {
        'default': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, 'proj.log'),
            'maxBytes': 1024 * 1024 * 3,  # 3 MB
            'backupCount': 5,
            'formatter': 'standard',
        },
        'request_handler': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, 'proj_request.log'),
            'maxBytes': 1024 * 1024 * 3,  # 3 MB
            'backupCount': 5,
            'formatter': 'standard',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'formatter': 'standard',
        }
    },
    'loggers': {
        'general': {
            'handlers': ['default'],
            'level': 'DEBUG',
            'propagate': False
        },
        'django.request': {
            'handlers': ['request_handler', 'mail_admins'],
            'level': 'ERROR',
            'propagate': False
        },
    }
}

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
        'rest_framework_datatables.renderers.DatatablesRenderer',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'rest_framework_datatables.filters.DatatablesFilterBackend',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework_datatables.pagination.DatatablesPageNumberPagination',
    'PAGE_SIZE': 2,
}

GRAPHENE = {
    'SCHEMA': 'proj.schema.schema'
}

MPTT_DEFAULT_LEVEL_INDICATOR = ''
MENU_SELECT_PARENTS = True
EXPLORER_CONNECTION_NAME = 'default'
EXPLORER_CONNECTIONS = {'Default': 'default'}
EXPLORER_DEFAULT_CONNECTION = 'default'
EXPLORER_PERMISSION_VIEW = lambda u: u.is_admin
EXPLORER_PERMISSION_CHANGE = lambda u: u.is_admin


# ---------------
# CELERY SETTINGS
# ---------------

# Get only one task at time
CELERY_TASK_ACKS_LATE = True
CELERY_WORKER_PREFETCH_MULTIPLIER = 1
CELERY_TRACK_STARTED = True

# Memory and process release
CELERYD_WORKER_MAX_TASKS_PER_CHILD = 20
CELERYD_WORKER_MAX_MEMORY_PER_CHILD = 781250  # 122070

# Serialization
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

# = * = * = * = * = * = * = * = * = * = * = * = * = * = *
# ENVIRONMENT LOADING CODE. No changes below this line! *
# = * = * = * = * = * = * = * = * = * = * = * = * = * = *

# ---

def get_environment():
    try:
        import __dev__
        return 'dev'
    except ImportError:
        try:
            import __staging__
            return 'staging'
        except ImportError:
            try:
                import __production__
                return 'production'
            except ImportError:
                return None


ACTUAL_ENV = get_environment()

if ACTUAL_ENV:
    try:
        if ACTUAL_ENV == 'dev':
            from .settings_dev import *
            if os.name == 'nt' and USE_SILK:
                INSTALLED_APPS = INSTALLED_APPS + ['silk']
                MIDDLEWARE = MIDDLEWARE + ['silk.middleware.SilkyMiddleware']
                SILKY_PYTHON_PROFILER = True
                SILKY_PYTHON_PROFILER_BINARY = True
                SILKY_META = True
        elif ACTUAL_ENV == 'staging':
            from .settings_staging import *
        elif ACTUAL_ENV == 'production':
            from .settings_production import *
    except ImportError:
        raise SystemError('Settings file not found for {0}! Missing settings_{0}.py file'.format(ACTUAL_ENV))
else:
    raise SystemError('Environment not set! Create __dev__.py, __staging__.py or __production__.py in source folder.')
