#!/usr/bin/env python
import os  # pragma: no cover

if __name__ == "__main__":  # pragma: no cover
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "proj.settings")

    try:
        import __dev__
        print('dev')
    except ImportError:
        try:
            import __staging__
            print('staging')
        except ImportError:
            try:
                import __production__
                print('production')
            except ImportError:
                print('ERROR! Environment not set. Add __dev__.py, __staging__.py or __production__.py to source folder')
                exit(1)

    exit(0)