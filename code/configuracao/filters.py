import django_filters
from dal import autocomplete

from configuracao.models import UnidadeFuncional, UnidadeEscolar, Turno, AnoLetivo


class UnidadeFuncionalFilter(django_filters.FilterSet):

    class Meta:
        model = UnidadeFuncional
        fields = {
            'nome': ['icontains'],
            'ativo': ['iexact']
        }


class UnidadeEscolarFilter(django_filters.FilterSet):

    class Meta:
        model = UnidadeEscolar
        fields = {
            'codigo': ['icontains'],
            'nome': ['icontains'],
            'ativo': ['iexact'],
        }


class TurnoFilter(django_filters.FilterSet):

    unidade_escolar = django_filters.ModelMultipleChoiceFilter(queryset=UnidadeEscolar.objects.all(),
                                                       field_name='unidade_escolar',
                                                       widget=autocomplete.ModelSelect2Multiple(
                                                         url='unidade-escolar-autocomplete',
                                                       )
                                                     )

    class Meta:
        model = Turno
        fields = {
            'descricao': ['icontains'],
        }


class AnoLetivoFilter(django_filters.FilterSet):

    class Meta:
        model = AnoLetivo
        fields = {
            'descricao': ['icontains'],
        }

