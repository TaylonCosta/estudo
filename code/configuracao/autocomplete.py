from dal import autocomplete

from configuracao.models import UnidadeFuncional, UnidadeEscolar, Turno, AnoLetivo


class UnidadeFuncionalAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):

        qs = UnidadeFuncional.objects.filter()

        if self.q:
            qs = qs.filter(nome__icontains=self.q)
        return qs


class UnidadeEscolarAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):

        qs = UnidadeEscolar.objects.filter()

        if self.q:
            qs = qs.filter(nome__icontains=self.q)
        return qs


class TurnoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):

        qs = Turno.objects.filter()

        if self.q:
            qs = qs.filter(descricao__icontains=self.q)
        return qs


class AnoLetivoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):

        qs = AnoLetivo.objects.filter()

        if self.q:
            qs = qs.filter(descricao__icontains=self.q)
        return qs

