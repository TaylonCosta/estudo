from django.urls import reverse_lazy

from configuracao.filters import UnidadeFuncionalFilter, UnidadeEscolarFilter, TurnoFilter, AnoLetivoFilter
from configuracao.forms import UnidadeFuncionalForm, UnidadeEscolarForm, TurnoForm, AnoLetivoForm
from configuracao.models import UnidadeFuncional, UnidadeEscolar, Turno, AnoLetivo
from configuracao.tables import UnidadeFuncionalTable, UnidadeEscolarTable, TurnoTable, AnoLetivoTable
from dashboard.forms import StackedFormSetHelper
from dashboard.views import DashboardListView, DashboardDeleteView, DashboardUpdateView, DashboardCreateView, \
    DashboardDetailView


class UnidadeFuncionalList(DashboardListView):

    page_title = 'Unidades Funcionais'
    header = 'Unidade funcional list'

    add_button_title = 'Nova unidade funcional'
    add_button_url = reverse_lazy('unidade-funcional-create')

    table_class = UnidadeFuncionalTable
    filter_class = UnidadeFuncionalFilter
    object = UnidadeFuncional

    def get_queryset(self):
        qs = UnidadeFuncional.objects.filter()
        return qs


class UnidadeFuncionalDetail(DashboardDetailView):

    page_title = 'Unidades Funcionais'
    header = 'Unidade funcional detail'
    object = UnidadeFuncional

    rows_based_on_form = UnidadeFuncionalForm

    def get_queryset(self):
        qs = UnidadeFuncional.objects.filter()
        return qs


class UnidadeFuncionalCreate(DashboardCreateView):

    page_title = 'Unidades Funcionais'
    header = 'Add unidade funcional'

    form_class = UnidadeFuncionalForm
    object = UnidadeFuncional

    show_button_save_continue = True
    owner_include = True

    success_message = 'Unidade funcional created successfully.'

    success_redirect = 'unidade-funcional-list'


class UnidadeFuncionalUpdate(DashboardUpdateView):

    page_title = 'Unidades Funcionais'
    header = 'Edit unidade funcional'

    form_class = UnidadeFuncionalForm
    object = UnidadeFuncional

    success_message = 'Unidade funcional updated successfully.'
    success_redirect = 'unidade-funcional-list'

    def get_queryset(self):
        qs = UnidadeFuncional.objects.filter()
        return qs


class UnidadeFuncionalDelete(DashboardDeleteView):

    success_message = 'Unidade funcional deleted successfully.'
    success_redirect = 'unidade-funcional-list'

    header = 'Delete unidade funcional'

    object = UnidadeFuncional
    validate_owner = False

    def get_queryset(self):
        qs = UnidadeFuncional.objects.filter()
        return qs

class UnidadeEscolarList(DashboardListView):

    page_title = 'Unidades Escolares'
    header = 'Unidade escolar list'

    add_button_title = 'New unidade escolar'
    add_button_url = reverse_lazy('unidade-escolar-create')

    table_class = UnidadeEscolarTable
    filter_class = UnidadeEscolarFilter
    object = UnidadeEscolar

    def get_queryset(self):
        qs = UnidadeEscolar.objects.filter()
        return qs


class UnidadeEscolarDetail(DashboardDetailView):

    page_title = 'Unidades Escolares'
    header = 'Unidade escolar detail'
    object = UnidadeEscolar

    rows_based_on_form = UnidadeEscolarForm

    def get_queryset(self):
        qs = UnidadeEscolar.objects.filter()
        return qs


class UnidadeEscolarCreate(DashboardCreateView):

    page_title = 'Unidades Escolares'
    header = 'Add unidade escolar'

    form_class = UnidadeEscolarForm
    object = UnidadeEscolar

    show_button_save_continue = True
    owner_include = True

    success_message = 'Unidade escolar created successfully.'

    success_redirect = 'unidade-escolar-list'


class UnidadeEscolarUpdate(DashboardUpdateView):

    page_title = 'Unidades Escolares'
    header = 'Edit unidade escolar'

    form_class = UnidadeEscolarForm
    object = UnidadeEscolar

    success_message = 'Unidade escolar updated successfully.'
    success_redirect = 'unidade-escolar-list'

    inlines = [
        {
            'title': "Turnos",
            'model': Turno,
            'form': TurnoForm,
            'helper': StackedFormSetHelper,
            'owner_include': True,
        }
    ]

    def get_queryset(self):
        qs = UnidadeEscolar.objects.filter()
        return qs


class UnidadeEscolarDelete(DashboardDeleteView):

    success_message = 'Unidade escolar deleted successfully.'
    success_redirect = 'unidade-escolar-list'

    header = 'Delete unidade escolar'

    object = UnidadeEscolar
    validate_owner = False

    def get_queryset(self):
        qs = UnidadeEscolar.objects.filter()
        return qs


class TurnoList(DashboardListView):

    page_title = 'Turnos'
    header = 'Turno list'

    add_button_title = 'New turno'
    add_button_url = reverse_lazy('turno-create')

    table_class = TurnoTable
    filter_class = TurnoFilter
    object = Turno

    def get_queryset(self):
        qs = Turno.objects.filter()
        return qs


class TurnoDetail(DashboardDetailView):

    page_title = 'Turnos'
    header = 'Turno detail'
    object = Turno

    rows_based_on_form = TurnoForm

    def get_queryset(self):
        qs = Turno.objects.filter()
        return qs


class TurnoCreate(DashboardCreateView):

    page_title = 'Turnos'
    header = 'Add turno'

    form_class = TurnoForm
    object = Turno

    show_button_save_continue = True
    owner_include = True

    success_message = 'Turno created successfully.'

    success_redirect = 'turno-list'


class TurnoUpdate(DashboardUpdateView):

    page_title = 'Turnos'
    header = 'Edit turno'

    form_class = TurnoForm
    object = Turno

    success_message = 'Turno updated successfully.'
    success_redirect = 'turno-list'

    def get_queryset(self):
        qs = Turno.objects.filter()
        return qs


class TurnoDelete(DashboardDeleteView):

    success_message = 'Turno deleted successfully.'
    success_redirect = 'turno-list'

    header = 'Delete turno'

    object = Turno
    validate_owner = False

    def get_queryset(self):
        qs = Turno.objects.filter()
        return qs


class AnoLetivoList(DashboardListView):

    page_title = 'Anos Letivos'
    header = 'Anos Letivos'

    add_button_title = 'Cadastrar ano letivo'
    add_button_url = reverse_lazy('ano-letivo-create')

    table_class = AnoLetivoTable
    filter_class = AnoLetivoFilter
    object = AnoLetivo

    def get_queryset(self):
        qs = AnoLetivo.objects.filter()
        return qs


class AnoLetivoDetail(DashboardDetailView):

    page_title = 'Anos Letivos'
    header = 'Anos Letivos'
    object = AnoLetivo

    rows_based_on_form = AnoLetivoForm

    def get_queryset(self):
        qs = AnoLetivo.objects.filter()
        return qs


class AnoLetivoCreate(DashboardCreateView):

    page_title = 'Anos Letivos'
    header = 'Add ano letivo'

    form_class = AnoLetivoForm
    object = AnoLetivo

    show_button_save_continue = True
    owner_include = True

    success_message = 'Anos Letivos cadastrado com sucesso.'

    success_redirect = 'ano-letivo-list'


class AnoLetivoUpdate(DashboardUpdateView):

    page_title = 'Anos Letivos'
    header = 'Editar ano letivo'

    form_class = AnoLetivoForm
    object = AnoLetivo

    success_message = 'Anos Letivos atualizado com sucesso.'
    success_redirect = 'ano-letivo-list'

    def get_queryset(self):
        qs = AnoLetivo.objects.filter()
        return qs


class AnoLetivoDelete(DashboardDeleteView):

    success_message = 'Anos Letivos removido com sucesso.'
    success_redirect = 'ano-letivo-list'

    header = 'Remover ano letivo'

    object = AnoLetivo
    validate_owner = False

    def get_queryset(self):
        qs = AnoLetivo.objects.filter()
        return qs


