from django_tables2 import columns

from configuracao.models import UnidadeFuncional, UnidadeEscolar, Turno, AnoLetivo
from dashboard import tables as dashboard_table

class UnidadeFuncionalTable(dashboard_table.Table):

    class Meta(dashboard_table.Table.Meta):
        model = UnidadeFuncional
        fields = ['codigo', 'tipo', 'nome', 'ativo']
        exclude = ('select_row', )


class UnidadeEscolarTable(dashboard_table.Table):

    class Meta(dashboard_table.Table.Meta):
        model = UnidadeEscolar
        fields = ['codigo', 'nome', 'ativo']
        exclude = ('select_row', )


class TurnoTable(dashboard_table.Table):

    class Meta(dashboard_table.Table.Meta):
        model = Turno
        fields = ['unidade_escolar', 'descricao']
        exclude = ('select_row', )


class AnoLetivoTable(dashboard_table.Table):

    class Meta(dashboard_table.Table.Meta):
        model = AnoLetivo
        fields = ['descricao', 'atual', 'ativo']
        exclude = ('select_row', )
