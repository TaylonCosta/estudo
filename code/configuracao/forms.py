from dal import autocomplete, forward
from django import forms

from configuracao.models import UnidadeFuncional, UnidadeEscolar, Turno, AnoLetivo


class UnidadeFuncionalForm(forms.ModelForm):

    class Meta:
        model = UnidadeFuncional
        fields = '__all__'
        exclude = ('created_by', 'created_on', 'modified_by', 'modified_on')


class UnidadeEscolarForm(forms.ModelForm):

    class Meta:
        model = UnidadeEscolar
        fields = '__all__'
        exclude = ('created_by', 'created_on', 'modified_by', 'modified_on')


class TurnoForm(forms.ModelForm):

    class Meta:
        model = Turno
        fields = '__all__'
        exclude = ('created_by', 'created_on', 'modified_by', 'modified_on')
        widgets = {
            'unidade_escolar': autocomplete.ModelSelect2(url='unidade-escolar-autocomplete'),
        }


class AnoLetivoForm(forms.ModelForm):

    class Meta:
        model = AnoLetivo
        fields = '__all__'
        exclude = ('created_by', 'created_on', 'modified_by', 'modified_on')
