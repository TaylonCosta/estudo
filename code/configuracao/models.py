from abc import abstractmethod

from django.db import models

from dashboard.models import DashboardBaseModel


class UnidadeFuncional(DashboardBaseModel):

    # TIPO_ESCOLA = 'e'
    # TIPO_OUTROS = 'o'
    #
    # TIPOS_CHOICES = (
    #     (TIPO_ESCOLA, 'Escola'),
    #     (TIPO_OUTROS, 'Outros'),
    # )

    codigo = models.SlugField()
    # tipo = models.CharField(choices=TIPOS_CHOICES, max_length=2)
    nome = models.CharField(max_length=200)
    ativo = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Unidade Funcional"
        verbose_name_plural = "Unidades Funcionais"
        ordering = ('nome',)

    def __str__(self):
        return self.nome

    @classmethod
    def set_filter_by_str(cls, q):

        filter = {'nome__iexact': q}

        return filter


class UnidadeEscolar(DashboardBaseModel):

    codigo = models.SlugField()
    nome = models.CharField(max_length=200)
    ativo = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Unidade Escolar"
        verbose_name_plural = "Unidades Escolares"
        ordering = ('nome',)

    def __str__(self):
        return self.nome

    @classmethod
    def set_filter_by_str(cls, q):

        filter = {'nome__iexact': q}

        return filter


class Turno(DashboardBaseModel):

    unidade_escolar = models.ForeignKey(UnidadeEscolar, on_delete=models.CASCADE)
    descricao = models.CharField(max_length=100)

    class Meta:
        ordering = ('unidade_escolar', 'descricao')

    def __str__(self):
        return self.descricao

    @classmethod
    def set_filter_by_str(cls, q):

        filter = {'descricao__iexact': q}

        return filter


class AnoLetivo(DashboardBaseModel):

    descricao = models.IntegerField(unique=True, verbose_name='Descrição')
    atual = models.BooleanField(default=True)
    ativo = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Ano Letivo'
        verbose_name_plural = 'Anos Letivos'
        ordering = ('descricao', )

    def __str__(self):
        return f'{self.descricao}'

    @classmethod
    def set_filter_by_str(cls, q):

        filter = {'descricao': q}

        return filter



