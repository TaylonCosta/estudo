from django.contrib.auth.decorators import login_required
from django.urls import path, include

from configuracao.autocomplete import UnidadeFuncionalAutocomplete, UnidadeEscolarAutocomplete, TurnoAutocomplete, \
    AnoLetivoAutocomplete
from configuracao.views import UnidadeFuncionalList, UnidadeFuncionalCreate, UnidadeFuncionalDetail, \
    UnidadeFuncionalUpdate, UnidadeFuncionalDelete, UnidadeEscolarList, UnidadeEscolarCreate, UnidadeEscolarDetail, \
    UnidadeEscolarUpdate, UnidadeEscolarDelete, TurnoList, TurnoCreate, TurnoDetail, TurnoUpdate, TurnoDelete, \
    AnoLetivoList, AnoLetivoCreate, AnoLetivoDetail, AnoLetivoUpdate, AnoLetivoDelete
from dashboard.views import DashboardIndexView
from users.decorators import register_resource

urlpatterns = [

    path('configuracao/', register_resource(DashboardIndexView.as_view(page_title='Configurações', header='Configurações', columns=[3, 3, 3, 3])), name='configuracao-index'),

    path('configuracao/unidades-funcionais/', include([
        path('', register_resource(UnidadeFuncionalList), name='unidade-funcional-list'),
        path('create/', register_resource(UnidadeFuncionalCreate), name='unidade-funcional-create'),
        path('detail/<int:pk>', register_resource(UnidadeFuncionalDetail), name='unidade-funcional-detail'),
        path('update/<int:pk>', register_resource(UnidadeFuncionalUpdate), name='unidade-funcional-update'),
        path('delete/<int:pk>', register_resource(UnidadeFuncionalDelete), name='unidade-funcional-delete'),
        path('autocomplete/', login_required(UnidadeFuncionalAutocomplete.as_view()), name='unidade-funcional-autocomplete'),
    ])),

    path('configuracao/unidades-escolares/', include([
        path('', register_resource(UnidadeEscolarList), name='unidade-escolar-list'),
        path('create/', register_resource(UnidadeEscolarCreate), name='unidade-escolar-create'),
        path('detail/<int:pk>', register_resource(UnidadeEscolarDetail), name='unidade-escolar-detail'),
        path('update/<int:pk>', register_resource(UnidadeEscolarUpdate), name='unidade-escolar-update'),
        path('delete/<int:pk>', register_resource(UnidadeEscolarDelete), name='unidade-escolar-delete'),
        path('autocomplete/', login_required(UnidadeEscolarAutocomplete.as_view()), name='unidade-escolar-autocomplete'),
    ])),

    path('configuracao/turnos/', include([
        path('', register_resource(TurnoList), name='turno-list'),
        path('create/', register_resource(TurnoCreate), name='turno-create'),
        path('detail/<int:pk>', register_resource(TurnoDetail), name='turno-detail'),
        path('update/<int:pk>', register_resource(TurnoUpdate), name='turno-update'),
        path('delete/<int:pk>', register_resource(TurnoDelete), name='turno-delete'),
        path('autocomplete/', login_required(TurnoAutocomplete.as_view()), name='turno-autocomplete'),
    ])),

    path('configuracao/anos-letivos/', include([
        path('', register_resource(AnoLetivoList), name='ano-letivo-list'),
        path('create/', register_resource(AnoLetivoCreate), name='ano-letivo-create'),
        path('detail/<int:pk>', register_resource(AnoLetivoDetail), name='ano-letivo-detail'),
        path('update/<int:pk>', register_resource(AnoLetivoUpdate), name='ano-letivo-update'),
        path('delete/<int:pk>', register_resource(AnoLetivoDelete), name='ano-letivo-delete'),
        path('autocomplete/', login_required(AnoLetivoAutocomplete.as_view()), name='ano-letivo-autocomplete'),
    ])),





]

