$(document).ready(function () {

    var workscope_status_list = $('.workscope-status')

    for (var i = 0; i < workscope_status_list.length; i++) {
        workscope_status_list[i].innerHTML = getWsStatusName(workscope_status_list[i].innerHTML)
    }

    var valor = document.getElementById('cyber-security').innerHTML
    valor.toString();
    if(!valor.includes("month")){
        document.getElementById("cyber-security").className = "pull-right badge bg-red"
    }

});

var table = {};

setTimeout(() => {
    table = $('#search_tasks').DataTable({
        "scrollX": true,
        "serverSide": true,
        "processing": true,
        "responsible": true,
        "autoWidth": false,
        "pageLength": 10,
        "language": {
            processing: '<i class="fa fa-refresh fa-spin"></i>'
        },
        "order": [[ 12, "desc" ]],
        "ajax": {
            "url": "/api/workscope/search/?format=datatables",
            "type": "POST",
            beforeSend: function (request) {
                request.setRequestHeader('X-CSRFToken', csrf_token);
            },
            "data": function (d) {
                return $.extend({}, d, {
                    'filter': JSON.stringify(filter)
                });
            },
            "error": function (response) {
                $.notify(response.responseJSON.data[0], {type: 'warning'});
            },
            "complete": function (a) {
                let recordsTotal = a.responseJSON.recordsTotal;
                $("#table-box").removeClass('hidden');
                if (recordsTotal == 0 || !allowedToShowTasks) {
                    $("#whitout-tasks").removeClass('hidden');
                    $("#search_tasks_wrapper").addClass('hidden');
                } else {
                    $("#whitout-tasks").addClass('hidden');
                    $("#search_tasks_wrapper").removeClass('hidden');
                }
            }
        },
        "drawCallback": function( settings ) {
            setTimeout(function () {
                table.columns.adjust();
            }, 1);
        },
        select: true,
        "columns": [
            {"data": "product_line", 'name': 'product_line__name'},
            {"data": "account", "name": "account__name"},
            {"data": "sites_header", "className": 'text-nowrap', 'name': 'sites__site_id'},
            {"data": "activity_type", "className": 'text-nowrap', 'name': 'activity_type__name'},
            {"data": "last_task_status", "className": 'text-nowrap',
                'name': 'scopemanager__scopemanageractivitystatus__activity_type_status__status_name__name'},
            {"data": "priority"},
            {"data": "due_date"},
            {
                "data": "scope_status_header", "className": 'text-nowrap',
                'name': 'scopemanager__scopemanageractivitystatus__activity_type_status__status_name__name'
            },
            {"data": "unique_extra_fields", 'name': 'unique_extra_fields',
                "name": 'productlineextrafieldlog__extra_field__field_name, activitytypeextrafieldlog__extra_field__field_name'},
            {"data": "technical_scenario", "className": 'text-nowrap', 'name': 'technical_scenario__name'},
            {"data": "project", 'name': 'project__name', "className": 'text-nowrap'},
            {"data": "updated_date", "className": 'text-nowrap', 'name': 'scopemanager__modified_on'},
            {"data": "tags_header", "className": 'text-nowrap', 'name': 'tags__identifier'},
            {"data": "du_header", "className": 'text-nowrap', 'name': 'related_du__du'}
        ],
        dom: '<"box-header"<"table_header"B><"table_header"l><"table_header"i>>rt<"table_header"p>p',
        buttons: [
            {
                extend: 'copy',
                text: '<i class="fa fa-copy"></i> Copy to clipboard',
                className: 'btn btn-sm btn-default',
                title: null,
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            },
            {
                extend: 'print',
                text: '<i class="fa fa-print"></i> Print',
                className: 'btn btn-sm btn-default',
                title: null,
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            }
        ]
    });
}, 1000);

function validateBeforeQuery(id) {
    let initialListFilter = scopetask[getScopeTaskStatusName(id)];

    allowedToShowTasks = initialListFilter.length > 0;

    filter.query = initialListFilter.map((ws_id) => '#' + ws_id).join('\n');
}

function updateTable(id) {

    id--;

    validateBeforeQuery(id);
    table.ajax.reload();
}
