import django_filters
from dal import autocomplete, forward

from dashboard.filters import UserNameFilter, CyberSecurityFilter, DateTimeRangeFilter
from dashboard.forms import DateRangeInput, DateTimeRangeInput
from users.models import User

FILTER_CHOICES = (
    (True,'Yes'),
    (False,'No'),
)

class UserFilter(django_filters.FilterSet):

    def is_valid(self):
        return super().is_valid()

    name = UserNameFilter(label='Name contains', field_name='')

    is_admin = django_filters.ChoiceFilter(choices=FILTER_CHOICES, label="User Admin")

    is_active = django_filters.ChoiceFilter(choices=FILTER_CHOICES, label="User Active")

    class Meta:
        model = User
        fields = ['name', 'email']

