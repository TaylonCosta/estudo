from django.contrib import admin
from .models import Role, RolePermission, UserResource


class RolePermissionAdminInline(admin.TabularInline):
    model = RolePermission


@admin.register(Role)
class RoleAdmin(admin.ModelAdmin):
    inlines = [
        RolePermissionAdminInline
    ]


@admin.register(UserResource)
class UserResourceAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'menu_type']
    list_filter = ['menu_type']
    search_fields = ['name', 'description']

