from dashboard import tables as dashboard_table
from django_tables2 import columns

from users.models import User, Role


class UserTable(dashboard_table.Table):

    name = columns.Column(empty_values=(), order_by=('first_name', 'last_name'))

    email = columns.Column(accessor='email')

    def render_name(self, record):
        return str(record)

    class Meta(dashboard_table.Table.Meta):
        model = User
        fields = ['name', 'email','profile.organization', 'profile.rh_level','profile.cyber_security_date', 'is_admin','is_active', 'last_login']
        # exclude = ('select_row', )


class RoleTable(dashboard_table.Table):

    class Meta(dashboard_table.Table.Meta):
        model = Role
        fields = ['description', 'active', ]
        exclude = ('select_row', )
