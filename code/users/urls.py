from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.urls import path, include, reverse_lazy
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView

from dashboard.forms import FormSetHelper
from users.apis import UserUpdateAvatarAPI
from users.autocomplete import UserAutocomplete, UserResourceAutocomplete, RoleAutocomplete
from users.decorators import register_resource
from users.views import UserNotification, UserProfileChangeView, UserListView, UserCreateView, UserDetailView, \
    UserUpdateView, UserDeleteView, UserUpdateAvatarView, RoleList, RoleCreate, RoleDetail, \
    RoleUpdate, RoleDelete, UserChangePassword

urlpatterns = [

    path('auth/', include([
        path('login/', auth_views.LoginView.as_view(template_name='auth/login.html', redirect_authenticated_user=True,
                                                    extra_context={'SystemShortName': settings.SYSTEM_SHORT_NAME}),
             name='login'),
        path('logout/', auth_views.LogoutView.as_view(), name='logout'),



        path('reset-password/', auth_views.PasswordResetView.as_view(template_name='auth/password_reset.html',
                                                                     extra_context={'helper': FormSetHelper,
                                                                                    'SystemShortName': settings.SYSTEM_SHORT_NAME}),
             name='password_reset'),

        path('reset-password-done/', auth_views.PasswordResetDoneView.as_view(template_name='auth/password_reset_done.html',
                                                    extra_context={'SystemShortName': settings.SYSTEM_SHORT_NAME}),
             name='password_reset_done'),

        path('reset-password-confirm/<uidb64>/<token>', auth_views.PasswordResetConfirmView.as_view(
            template_name='auth/password_reset_confirm.html',
            extra_context={'helper': FormSetHelper, 'SystemShortName': settings.SYSTEM_SHORT_NAME}),
            name='password_reset_confirm'),

        path('reset-password-complete/', auth_views.PasswordResetCompleteView.as_view(
            template_name='auth/password_reset_complete.html',
            extra_context={'SystemShortName': settings.SYSTEM_SHORT_NAME}),
            name='password_reset_complete'),

        path('notifications', login_required(UserNotification.as_view()), name='user-notifications'),
        path('autocomplete/', login_required(UserAutocomplete.as_view()), name='user-autocomplete'),
    ])),

    path('profile/', include([
        path('', include([
                path('', register_resource(
                    TemplateView.as_view(template_name='dashboard/index_page.html', extra_context={'title': 'Profile'}),
                    as_view=False, resource='profile.dashboard', description="Profile Dashboard"), name='profile-dashboard'),

            ])),
        path('avatar/', login_required(UserUpdateAvatarView.as_view()), name='profile-avatar-update'),
        path('change-user/', login_required(UserProfileChangeView.as_view()), name='profile-user-change'),
        path('change-password/', auth_views.PasswordChangeView.as_view(success_url=reverse_lazy('profile-password-done'),template_name='auth/password_change.html',
                                                                       extra_context={'helper': FormSetHelper}),

             name='profile-password-change'),
        path('change-password-done/',
             auth_views.PasswordChangeDoneView.as_view(template_name='auth/password_change_done.html'),
             name='profile-password-done'),
    ])),


    path('people/', include([
        path('', register_resource(UserListView), name='users-profile-list'),
        path('create/', register_resource(UserCreateView), name='users-profile-create'),
        path('detail/<int:pk>', register_resource(UserDetailView), name='users-profile-detail'),
        path('update/<int:pk>', register_resource(UserUpdateView), name='users-profile-update'),
        path('delete/<int:pk>', register_resource(UserDeleteView), name='users-profile-delete'),
        path('change-password/', login_required(UserChangePassword.as_view()), name='users-change-password'),

    ])),

    path('people/roles/', include([
        path('', register_resource(RoleList), name='users-role-list'),
        path('create/', register_resource(RoleCreate), name='users-role-create'),
        path('detail/<int:pk>', register_resource(RoleDetail), name='users-role-detail'),
        path('update/<int:pk>', register_resource(RoleUpdate.as_view(), as_view=False, resource='roles.update', description="Update Roles"), name='users-role-update'),
        path('delete/<int:pk>', register_resource(RoleDelete), name='users-role-delete'),
        path('autocomplete/', login_required(RoleAutocomplete.as_view()), name='role-autocomplete'),
        path('resources/autocomplete/', login_required(UserResourceAutocomplete.as_view()), name='userresource-autocomplete'),
    ])),

    path('api/', include([
        path('users/avatar/update/', UserUpdateAvatarAPI.as_view(), name='api-users-avatar-update'),
    ])),
]
