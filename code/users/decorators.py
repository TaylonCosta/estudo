from django.contrib.auth.decorators import user_passes_test, login_required
from django.core.exceptions import PermissionDenied

from dashboard.permissions import baseResource


def require_resource_permission(resource):
    '''
    Decorator for views that checks that the logged in user has a resource allowed
    '''

    def check_user_type(user):
        user_roles = user.get_allowed_resources()

        if resource not in user_roles and not user.is_admin:
            raise PermissionDenied()

        return True

    return user_passes_test(check_user_type)


# def register_resource(resource, description, function):
#     '''
#     Decorator for register resource
#     '''
#
#     baseResource.register(resource, description)
#
#     return function

def register_resource(cls, require_login=True, as_view=True, resource=None, description=None):
    '''
    Decorator for register resource
    '''

    as_view = bool(type(cls).__name__ != 'function')

    if as_view:

        resource_name_cls = cls.get_resource()

        if resource_name_cls is not None:
            resource = resource_name_cls
            description = cls.header
            if resource and description:
                baseResource.register(resource, description)
                if cls.resource_type == 'list':
                    # register export resources
                    if cls.show_export_button:
                        baseResource.register(f'{resource_name_cls.split(".")[0]}.export', f'{description} Export')
                    if cls.allow_export_import:
                        baseResource.register(f'{resource_name_cls.split(".")[0]}.template', f'{description} Template Import/Export')

    elif resource and description:
        baseResource.register(resource, description)

    if as_view:
        return_function = cls.as_view()
    else:
        return_function = cls

    if require_login:
        return login_required(return_function)
    else:
        return return_function
