let csrfToken = '';

function humanizeMomentDuration(duration) {
    let formatedDate = '';

    formatedDate = formatedDate + (duration._data.years > 0 ? duration._data.years + (duration._data.years == 1 ? ' year, ' : ' years, ') : '');
    formatedDate = formatedDate + (duration._data.months > 0 ? duration._data.months + (duration._data.months == 1 ? ' month, ' : ' months, ') : '');
    formatedDate = formatedDate + (duration._data.days > 0 ? duration._data.days + (duration._data.days == 1 ? ' day, ' : ' days, ') : '');
    formatedDate = formatedDate + (duration._data.hours < 10 ? '0' + duration._data.hours + ':' : duration._data.hours + ':');
    formatedDate = formatedDate + (duration._data.minutes < 10 ? '0' + duration._data.minutes + ':' : duration._data.minutes + ':');
    formatedDate = formatedDate + (duration._data.seconds < 10 ? '0' + duration._data.seconds : duration._data.seconds);

    return formatedDate;
}

let filterLoaded = false;
let filterLoading = false;

setTimeout(() => { filterLoaded = true; }, 2000);

function clearField(parent, fields) {

    if (!filterLoaded || filterLoading) {
        return;
    }

    let parent_name = parent.name.split('-').pop();

    let fields_to_clear = fields.split(',');

    for (index in fields_to_clear) {
        var field_to_clear = parent.name.replace(parent_name, fields_to_clear[index]);
        $('#id_'+field_to_clear).val(null).trigger("change");
        $('#id_'+field_to_clear+' option').remove();
    }

}

$.notifyDefaults({
    type: 'info',
    delay: 2000,
    offset: {
        x: 20,
        y: 60
    }
});

$(document).ajaxStart(function() { Pace.restart(); });

$(document).ready(function () {

        $('[data-mask]').inputmask();

});


// Applied globally on all textareas with the "autoExpand" class
$(document)
    .one('focus.autoExpand', 'textarea.autoExpand', function(){
        var savedValue = this.value;
        this.value = '';
        this.baseScrollHeight = this.scrollHeight;
        this.value = savedValue;
    })
    .on('input.autoExpand', 'textarea.autoExpand', function(){
        var minRows = this.getAttribute('data-min-rows')|0, rows;
        this.rows = minRows;
        rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 16);
        this.rows = minRows + rows;
    });

$('.date-range').daterangepicker({
    opens: 'center',
    autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear',
          format: 'YYYY-MM-DD'
      }
});

$('.date-range').on('apply.daterangepicker', function(ev, picker) {
    if (picker.startDate.format('YYYY-MM-DD') === picker.endDate.format('YYYY-MM-DD')) {

        $(this).val(picker.startDate.format('YYYY-MM-DD'));

    } else {
        $(this).val(picker.startDate.format('YYYY-MM-DD') + ' until ' + picker.endDate.format('YYYY-MM-DD'));
    }
});

$('.date-range').on('cancel.daterangepicker', function(ev, picker) {
  $(this).val('');
});

$('.date-time-range').daterangepicker({
    opens: 'left',
    timePicker: true,
    autoUpdateInput: false,
    timePicker24Hour: true,
      locale: {
          cancelLabel: 'Clear',

      }
});


$('.date-time-range').on('apply.daterangepicker', function(ev, picker) {
  $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm') + ' until ' + picker.endDate.format('YYYY-MM-DD HH:mm'));
});

$('.date-time-range').on('cancel.daterangepicker', function(ev, picker) {
  $(this).val('');
});

$('.select2').select2({
    allowClear: true
});

function showModalError(data) {
    const image = '<img  class="img" width="100%" height="100%" src="data:image/png;base64,'+ data +'">';
    document.getElementById('content-img-error').innerHTML = image;
    $('#modal-doc-error').modal('show');
}


function showModalErrorApp(param) {
    var b64 = $(param).children()[0].getAttribute('src')
    var image = '<img  class="img" width="100%" height="100%" src="'+b64+'">';
    document.getElementById('content-img-error').innerHTML = image;
    $('#modal-app-error').modal('show');
}


function cleanNonSelectSideBarItem() {
    if ($('.main-sidebar').find('li.active').length > 1) {
        $('.main-sidebar').find('li.active').first().removeClass('active');
    }
    $.holdReady( false );
}
$.holdReady( true );
cleanNonSelectSideBarItem();

function footer_on_bottom() {
    let minContentSize = 0;
    let actualContentSize = 0;

    // when no header
    if ($('.content-header').length == 0) {
        minContentSize = window.innerHeight - 120;
        actualContentSize = document.getElementsByClassName('content-wrapper')[0].offsetHeight;
    } else {
        minContentSize = window.innerHeight - 195;
        actualContentSize = document.getElementsByClassName('content')[0].offsetHeight;
    }

    $('.content').attr('style','min-height:' + minContentSize + 'px;');

    $('.main-footer').show();
}

$('.content-wrapper').scroll(function() {

    var btn = $('#back_to_top');

    if ($('.content-wrapper').scrollTop() > 500) {
        btn.addClass('show');
    } else {
        btn.removeClass('show');
    }
});

 $('#back_to_top').on('click', function(e) {
    e.preventDefault();
    $('.content-wrapper').animate({scrollTop:0, scrollLeft:0}, '300');
});

function clickOn(elementId) {

    if (!elementId) {
        return;
    }

    let element = $('#' + elementId);

    if (element.length == 0) {
        return;
    }

    element.click();
}


/**********************************************************************************************************************/
/*                                                                                                                    */
/*                                                                                                                    */
/*      Content Iframe Algorithm                                                                                      */
/*                                                                                                                    */
/*                                                                                                                    */
/**********************************************************************************************************************/

function show_content_on_iframe(page_id, page_url) {
    start_spinner();
    hide_current_content(page_id);

    if (is_page_already_rendered(page_id)) {
        show_page(page_id);
    } else {
        let new_content_iframe = '<iframe id="' + page_id + '" ' +
                                    ' src="' + page_url + '" '  +
                                    ' class="inner_page"> ' +
                             '</iframe> ';

        $('#iframe-section').append(new_content_iframe);
    }

    select_sidebar_menu(page_id);
    update_browser_url(page_url);

    setTimeout( function hide_spinner_when_iframe_content_is_loaded(){ stop_spinner(); }, 2000);


}

function hide_current_content(page_id) {
    $('.content-wrapper').css('overflow-y', 'hidden');

    $('.inner_page').each( (index, value) => {
        if ($(value).attr('id') != page_id) {
            $(value).hide();
        } else {
             $(value).show();
        }
    });

    $('.content-header').remove();
    $('.content').remove();
}

function is_page_already_rendered(page_id) {
    return $('#iframe-section').find('#' + page_id).length == 1;
}

function select_sidebar_menu(page_id) {
    $('.main-sidebar').find('li.active').removeClass('active');

    $('.main-sidebar').find('#' + page_id).addClass('active');
}

function update_browser_url(page_url, url_extra = null) {
    let new_url = page_url;

    if (url_extra) {
        new_url += "/" + url_extra;
    }

    // Verify if Browser supports pushState() method
    if (window.history.pushState) {
        window.history.replaceState('', '', new_url);
    } else {
        // Used by old versions browser
        document.location.href = "/" + new_url;
    }
}

function show_page(page_id) {
    $('#' + page_id).show();
}

function start_spinner() {
    $('#iframe-loading').show();
}

function stop_spinner() {
    $('#iframe-loading').hide();
}

/**********************************************************************************************************************/
/*                                                                                                                    */
/*                                                                                                                    */
/*      TV mode (Full screen on content)                                                                              */
/*                                                                                                                    */
/*                                                                                                                    */
/**********************************************************************************************************************/

let tv_mode = false;

document.addEventListener('fullscreenchange', exitHandler, false);

function exitHandler() {
        if (!document.fullscreen && !document.webkitIsFullScreen && !document.mozFullScreen
            && (document.msFullscreenElement == null || document.msFullscreenElement == undefined)) {
            show_all_elements();
         }
}


$( document ).ready(function() {
	$("#tv_mode").click(function() {
		if(tv_mode == false) {
			show_only_content_element();
			start_tv_mode();
		} else {
			show_all_elements();
			stop_tv_mode();
		}
	});
});

function show_only_content_element() {
    $(".main-header").hide();
    $(".main-sidebar").hide();
    $(".main-footer").hide();

    $('.content-wrapper').attr('style', 'background-color: #ECF0F5; margin-left: 0px !important');
    $('.wrapper').attr('style', 'margin-top: 0px !important; min-height: 110%;');

    tv_mode = true;
    $("#tv_mode > i").addClass("fa-expand");
    $("#tv_mode > i").removeClass("fa-compress");
}

function show_all_elements() {
    $(".main-header").show();
    $(".main-sidebar").show();
    $(".main-footer").show();

    $('.wrapper').attr('style', 'margin-top: 52px !important; min-height: 100%;');

    tv_mode = false;
    $("#tv_mode > i").removeClass("fa-compress");
    $("#tv_mode > i").addClass("fa-expand");
}

function start_tv_mode() {
	if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) {
		if (document.documentElement.requestFullscreen) {
			document.documentElement.requestFullscreen();
		} else if (document.documentElement.msRequestFullscreen) {
			document.documentElement.msRequestFullscreen();
		} else if (document.documentElement.mozRequestFullScreen) {
			document.documentElement.mozRequestFullScreen();
		} else if (document.documentElement.webkitRequestFullscreen) {
			document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
		}
	}
}

function stop_tv_mode() {
	if (document.exitFullscreen) {
		document.exitFullscreen();
	} else if (document.msExitFullscreen) {
		document.msExitFullscreen();
	} else if (document.mozCancelFullScreen) {
		document.mozCancelFullScreen();
	} else if (document.webkitExitFullscreen) {
		document.webkitExitFullscreen();
	}
}

/**********************************************************************************************************************/
/*                                                                                                                    */
/*                                                                                                                    */
/*      Tooltip                                                                                                       */
/*                                                                                                                    */
/*                                                                                                                    */
/**********************************************************************************************************************/

function addTooltip(value, tooltipText) {
    let elm = $(value);

    elm.addClass('has-tooltip');
    elm.append('<span  class="tooltip-text">' + tooltipText + '</span>');
}

function startupTooltips() {
    let elements_to_add_tooltip = $('[tooltip]');
    elements_to_add_tooltip.each((index, value) => addTooltip(value, $(value).attr('tooltip')));
}


/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                                                                                                                    */
/*                                                                                                                    */
/*      DataTable Utils                                                                                               */
/*                                                                                                                    */
/*                                                                                                                    */
/**********************************************************************************************************************/

function parseArrayOfObjectsToArraysOfArrays(objectList) {
    return objectList.forEach((object) => parseObjectToArray(object));
}

function parseObjectToArray(object) {
    return [Object.keys(object).map((key) => object[key] )];
}

/**********************************************************************************************************************/

function adjustSidebarLayoutOnSmallWindow() {
    var body = $('body');

     if (window.innerWidth < 767) {
        body.removeClass('sidebar-collapse');
     }
}

function setContentNoScroll() {
    if (document.querySelectorAll('.tabbed-box, #table-template').length == 2) {
        document.querySelector('.content-wrapper').style.overflow = 'hidden';
    }
}

function setDataTableHeight() {

    let dataTableElements = document.querySelectorAll(".dataTables_scrollBody");
    let tabbedBoxElement = document.querySelector('.tabbed-box');

    if (dataTableElements.length == 0 || !tabbedBoxElement) {
        return;
    }

    let tabbedBoxHeight = 0;

    let tabbedBoxMinimizedBtn = document.querySelector('.nav-filter-button .fa.fa-chevron-left');

    if (tabbedBoxElement) {
        let height = tabbedBoxElement.offsetHeight;
        tabbedBoxHeight = tabbedBoxMinimizedBtn ? (height - 20) : height;
    }

    let tableMaxHeight = (window.innerHeight - tabbedBoxHeight - 205) + "px";

    dataTableElements.forEach((dataTableElement) => {
        dataTableElement.style.maxHeight = tableMaxHeight;
        dataTableElement.style.overflow = 'scroll';
    });
}

function setTableHeight() {
    let tableElement = document.querySelector('.table-container.table-responsive');

    if (!tableElement) {
        return;
    }

    let tabbedBoxElement = document.querySelector('.tabbed-box');
    let paginationElement = document.querySelector('.pagination');
    let tableHeight = window.innerHeight - ((tabbedBoxElement ? tabbedBoxElement.offsetHeight : 0) + (paginationElement ? 154 : 115));

    tableElement.style.maxHeight = tableHeight + 'px';
}

function setTablesHeight() {
    setTableHeight();
    setDataTableHeight();
}

$(window).resize(() => {
    setDataTableHeight();
});

// ------------------------------
// On Document Ready
// ------------------------------

document.addEventListener("DOMContentLoaded", () => {
    document.querySelectorAll('.tabbed-box #filter_badges li').forEach((element) => {
        element.addEventListener('click', () => {
            setTimeout(() => { setTablesHeight(); }, 50);
        });
    });

    $('.nav-filter-button').on('click', () => { setTimeout(() => { setTablesHeight(); }, 50); });
    $('[data-toggle="tooltip"]').tooltip();
    startupTooltips();

    // Add event click on calendar icons to show date range picker
    $(".input-group i.fa.fa-calendar").each(function( index, value ) {
        $(value).parent().bind( "click", {
              element_id: $(value).parent().parent().find('input').attr('id')
            }, function( event ) {
              clickOn(event.data.element_id);
        });
    });

    $('.textarea-wysi').wysihtml5({
        events: {
            "load": function () {
                 $(".wysihtml5-sandbox").css("width", "100%");
            },
        },
     });

    adjustSidebarLayoutOnSmallWindow();
    setTimeout(() => { setTablesHeight(); }, 1000);
    setContentNoScroll();
});