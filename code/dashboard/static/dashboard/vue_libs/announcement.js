var announcement = new Vue({
    el: '#announcement',
    delimiters: ['[[', ']]'],
    data: {
        newAnnouncements: [],
        unreadCount: 0,
    },

    methods: {
        getAnnouncements() {
            axios.get('/api/users/announcements/', {
            }).then(response => {

            if (!response.data) {
                return;
            }

            this.newAnnouncements = [];
            this.unreadCount = this.newAnnouncements.push(...response.data.map((announcement) => {
                announcement.publish_on = moment(announcement.publish_on).format('MMM. D, YYYY h:mm A');
                return announcement;
            }));

            }).catch((error) => {
                console.log(error);
            });

        },

        getIconClassFromAnnouncementType(type) {
            return getIconClassFromAnnouncementType(type);
        },

        showAnnouncementModal(announcement) {
            announcementModal.setAnnouncement(announcement, true);
        },

        showModal() {
            showModalElement('#announcement-modal');
        },

        hideModal() {
            hideModalElement('#announcement-modal');
        },

        announcementDescription(announcement) {
            let announcementDescriptionElement = $('#' + announcement.id + ' .announcement-description');

            if (announcementDescriptionElement.length == 0) {
                return;
            }

            announcementDescriptionElement.empty();
            announcementDescriptionElement.html(announcement.description);
        },
    },

    mounted() {
        this.getAnnouncements();
    }
});

var announcementModal = new Vue({
    el: '#announcement-modal',
    delimiters: ['[[', ']]'],
    data: {
        announcement: '',
        displayAwareButton: true,
    },

    methods: {
        getIconClassFromAnnouncementType(type) {
            return getIconClassFromAnnouncementType(type);
        },

        hideCard() {
            let announcementId = this.announcement.id;

            this.announcement = null;
            hideCard(announcementId);
            hideModalElement('#announcement-modal');
        },

        setAnnouncement(announcement, displayAwareButton) {
            this.displayAwareButton = displayAwareButton;
            announcement.publish_on = moment(announcement.publish_on).format('MMM. D, YYYY h:mm A');
            showModalElement('#announcement-modal');

            this.announcement = announcement;
        },
    },
});

var announcementModalOpen = new Vue({
    el: '#announcement-modal-open',
    delimiters: ['[[', ']]'],
    data: {
        announcements: [],
        unreadCount: 0,
    },

    methods: {

        hideCard(idAnnouncement) {
            hideCard(idAnnouncement);
        },

        hibernateAnnouncements() {
            this.announcements.forEach((announcement) => {
                hibernateAnnouncement(announcement.id);
            });
        },

        getAnnouncements() {
            axios.get('/api/users/announcements/', {
            }).then(response => {

            if (!response.data) {
                return;
            }

            let announcements = response.data.filter((announcement) => !isAnnouncementHibernated(announcement.id));

            this.announcements = [];
            this.unreadCount = this.announcements.push(...announcements.map((announcement) => {
                announcement.publish_on = moment(announcement.publish_on).format('MMM. D, YYYY h:mm A');
                return announcement;
            }));

            }).catch((error) => {
                console.log(error);
            });
        },

        getIconClassFromAnnouncementType(type) {
            return getIconClassFromAnnouncementType(type);
        },

        announcementDescription(announcement) {
            let announcementDescriptionElement = $('#' + announcement.id + ' .announcement-card__description');

            if (announcementDescriptionElement.length == 0) {
                return;
            }

            announcementDescriptionElement.empty();
            announcementDescriptionElement.html(announcement.description);
        },

    },
    mounted() {
        this.getAnnouncements();
    }
});