var MapControl = (function(){

    splitScreen = function(){
        Split(['#map_container', '#info_area_container'], {
            gutterSize: 10,
            sizes: [53, 32.5],
            direction: 'vertical',
            minSize: [100, 50]
        });
    };

    setEvents = function() {

        // Map resize
        $(window).on("resize", function () {
            map_control.resizeMap();
            info_area_control.handleAreasHeight();
        }).trigger("resize");

        // Split bar change
        $('.gutter').mousemove(function() {
            map_control.resizeMap();
            info_area_control.handleAreasHeight();
        });
    };

    init = function() {
        splitScreen();
        setEvents();
        map_control.resizeMap();
    }

    return {
        init: init
    }
})();

$(document).ready(function(){
    MapControl.init();
});