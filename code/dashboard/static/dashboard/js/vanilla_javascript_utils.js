
function getElementHeight(query) {

    if (!query) {
        return;
    }

    let element = document.querySelector(query);

    return element ? element.offsetHeight : 0;
}

function hideElement(query) {

    if (!query) {
        return;
    }

    let element = document.querySelector(query);

    if (!element) {
        return;
    }

    element.hidden = true;
}

function showElement(query) {

    if (!query) {
        return;
    }

    let element = document.querySelector(query);

    if (!element) {
        return;
    }

    element.hidden = false;
}

function hideModalElement(query) {

    if (!query) {
        return;
    }

    let element = document.querySelector(query);

    if (!element || !element.classList.contains('in')) {
        return;
    }

    element.classList.remove('in');
    element.style.display = 'none';

    document.querySelector('.content').style.filter = 'blur(0px)';

    let modalBackdropElement = document.querySelector('.modal-backdrop.fade.in');

    if (!modalBackdropElement) {
        return;
    }

    modalBackdropElement.remove();
}

function showModalElement(query) {

    if (!query) {
        return;
    }

    let element = document.querySelector(query);

    if (!element) {
        return;
    }

    document.querySelectorAll(query + ' [data-dismiss="modal"]').forEach((elm) => {
        elm.removeAttribute('data-dismiss');
        elm.addEventListener('click', () => { hideModalElement(query); });
    });

    element.classList.add('in');
    element.style.display = 'block';

    document.querySelector('.content').style.filter = 'blur(3px)';

    let modalBackdropHtml = '<div class="modal-backdrop fade in"></div>';
    let modalBackdropElement = document.createRange().createContextualFragment(modalBackdropHtml);

    document.querySelector('.wrapper').appendChild(modalBackdropElement);
}

function disableElement(query) {

    if (!query) {
        return;
    }

    let element = document.querySelector(query);

    if (!element) {
        return;
    }

    element.disabled = true;
}

function enableElement(query) {

    if (!query) {
        return;
    }

    let element = document.querySelector(query);

    if (!element) {
        return;
    }

    element.disabled = false;
}

// TODO-DEV: Find a alternative to replace jQuery datePicker
function clearDatePickerInput(query) {

    if (!query) {
        return;
    }

    let element = $(query);

    if (element.length == 0) {
        return;
    }

    element.data("DateTimePicker").clear();
}

// TODO-DEV: Find a lib to replace jQuery notify
function notify(message, type) {

    if (!message) {
        return;
    }

    if ('danger' === type) {
        $.notify(message, {type: 'danger'});
    } else if ('warning' === type) {
        $.notify(message, {type: 'warning'});
    } else if ('info' === type) {
        $.notify(message, {type: 'info'});
    } else {
        $.notify(message, {type: 'success'});
    }
}

// TODO-DEV: Find a lib to replace $.dataTables
