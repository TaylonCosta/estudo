const TYPE_INFO = 'i';
const TYPE_CHANGELOG = 'c';
const TYPE_MAINTENANCE = 'm';
const TYPE_URGENT = 'u';

function getIconClassFromAnnouncementType(type) {

    if (TYPE_INFO == type) {
        return 'fa fa-info-circle announcement-type-info';
    }

    if (TYPE_CHANGELOG == type) {
        return 'fa fa-refresh announcement-type-changelog';
    }

    if (TYPE_MAINTENANCE == type) {
        return 'fa fa-wrench announcement-type-maintenance';
    }

    if (TYPE_URGENT == type) {
        return 'fa fa-warning announcement-type-urgent';
    }

    return 'fa fa-circle-o';
}

function hideCard(idAnnouncement) {

    if (!idAnnouncement) {
        return;
    }

    let dataRequest = {'id_announcement': idAnnouncement};

    axios.post('/api/users/announcements/', dataRequest, {
        headers: {'X-CSRFToken': csrfToken,}
    }).then(response => {
        announcementModalOpen.getAnnouncements();
        announcement.getAnnouncements();

        setTimeout(() => {
            if (announcementModalOpen.announcements.length == 0) {
                hideModalElement('#announcement-modal-open');
            }
            if (!announcementModal.announcement) {
                hideModalElement('#announcement-modal');
            }
        }, 500);

    }).catch((error) => {
        console.log(error);
     });
}

function hibernateAnnouncement(announcementId) {
    sessionStorage.setItem(announcementId, Date.now());
}

function isAnnouncementHibernated(announcementId) {
    let timeHibernated = sessionStorage.getItem(announcementId);

    if (!timeHibernated) {
        return false;
    }

    let oneMinuteInMilliseconds = 60000;

    return (Date.now() - timeHibernated) < ( oneMinuteInMilliseconds * 10 );
}

document.addEventListener("DOMContentLoaded", () => {
    setTimeout(() => {
        if (announcementModalOpen.announcements.length > 0) {
            showModalElement('#announcement-modal-open');
        }
    }, 1000);
});