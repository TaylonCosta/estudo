import json
from difflib import SequenceMatcher
from io import BytesIO

import pandas as pd
from abc import abstractmethod

from crispy_forms.utils import render_crispy_form
from django.conf import settings
from django.contrib import messages
from django.contrib.gis.geos import Polygon
from django.core.exceptions import ValidationError, PermissionDenied
from django.db import transaction
from django.db.models import ProtectedError, DateField, DateTimeField

from django.forms import inlineformset_factory, all_valid
from django.http import HttpResponse, JsonResponse, HttpResponseForbidden
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.utils import timezone, text
from django.template.loader import render_to_string
from django.views import View
from django_tables2 import RequestConfig, LazyPaginator, A
from django_tables2.export import TableExport

import plotly.graph_objs as go
import plotly.offline as opy
from menu import Menu

from menu.templatetags.menu import MenuNode
# from silk.profiling.profiler import silk_profile

from dashboard.forms import FormSetHelper, FieldsColumnsFormSetHelper
from dashboard.utils import check_dashboard_permission


def find_selected_menu(context):
    # if a 500 error happens we get an empty context, in which case
    # we should just return to let the 500.html template render
    if 'request' not in context:
        return '<!-- menu failed to render due to missing request in the context -->'

    request = context['request']
    menus = Menu.process(request)

    found_item = {'item': None, 'similarity': 0}

    def iterate_over_items(items):
        for item in items:
            if item.selected:
                similarity = SequenceMatcher(None, request.path, item.url).ratio()
                if not found_item['item'] or similarity > found_item['similarity']:
                    found_item['item'] = item
                    found_item['similarity'] = similarity
            if item.children:
                iterate_over_items(item.children)

    for menu in menus:
        iterate_over_items(menus[menu])

    # set the items in our context
    return found_item['item']


class DashboardBaseView(View):

    resource_type = None
    resource = None

    page_title = 'Page Title'
    header = 'Page Header'

    object = None
    template_name = None
    extra_context = {}

    render_template = ''
    request = None

    def get_context(self):
        pass

    def get_extra_context(self):
        return self.extra_context

    def get(self, request):
        self.request = request
        context = self.get_render_context()
        return render(request, self.render_template, context)

    @classmethod
    def get_resource(cls, type_name=None):

        type_name = type_name or cls.resource_type

        if cls.resource_type in ['report', 'smart_report', 'chart']:
            return f'{cls.resource}.{type_name or cls.resource_type}'
        if cls.resource:
            return f'{cls.resource}.{type_name}'
        elif cls.object:
            return f'{cls.object._meta.model_name}.{type_name}'
        else:
            return None

    def _get_base_context(self):

        extra_content = self.get_extra_context()
        self.extra_context.update(extra_content)

        return {
            'SystemShortName': settings.SYSTEM_SHORT_NAME,
            'SystemLongName': settings.SYSTEM_LONG_NAME,
            'PageTitle': self.page_title,
            'Header': self.header,
            'ExtraContext': self.extra_context,
        }

    def get_render_context(self):

        data = self._get_base_context()

        try:
            data.update(self.get_context())
        except:
            pass

        data['menu_info'] = find_selected_menu({'request': self.request})

        return data


class DashboardIndexView(DashboardBaseView):

    resource_type = 'index'

    columns = [6, 6]

    def get_context(self):

        context = {'request': self.request}
        MenuNode().render(context)
        del(context['request'])

        cols_num = len(self.columns)
        menu_items = [{'col_size': self.columns[i], 'items': []} for i in range(cols_num)]

        c = 0
        column = 0

        for item in context['menus']['sidebar']:
            if item.area == context['selected_menu'].area and item.list:
                c += 1
                if c % cols_num != 0:
                    menu_items[column]['items'].append(item)
                    column += 1
                else:
                    menu_items[column]['items'].append(item)
                    column = 0

        context.update({'menu_items': menu_items})

        return context

    def get(self, request, *args, **kwargs):
        return render(request, 'dashboard/index_page.html', self.get_render_context())


class DashboardListView(DashboardBaseView):

    resource_type = 'list'

    add_button_title = 'New'
    add_button_url = ''
    columns = {}
    options = []
    actions = {}
    template_name = 'dashboard/list.html'
    template_name_table = 'dashboard/list_table.html'
    table_class = None
    filter_class = None
    lazy_pagination = False
    per_page = 50

    show_option_detail = True
    show_option_update = True
    show_option_delete = True
    show_export_button = True
    allow_export_import = False
    allow_insert_by_template = True
    allow_delete_by_template = True

    send_filtered_qs_to_template = False

    custom_template_key = {}
    custom_template_dtype = {}

    filter_form_helper = FieldsColumnsFormSetHelper

    form_field_lg_size = 3
    form_field_sm_size = 3
    form_field_md_size = 3
    form_field_xs_size = 12

    form_template_class = None

    @abstractmethod
    def get_queryset(self):
        pass

    def get_table_class(self, qs):
        return self.table_class(qs)

    def get_context(self):

        data = self._get_base_context()

        self.options = []

        if self.add_button_title and self.add_button_url:
            data['AddButton'] = {}
            data['AddButton']['Title'] = self.add_button_title
            data['AddButton']['Url'] = self.add_button_url
        if self.show_export_button:
            data['ExportButton'] = True
        if self.allow_export_import:
            data['TemplateButtons'] = True
            data['ColumnsToIgnore'] = self.get_columns_to_ignore_on_template()
        if self.columns:
            data['Columns'] = self.columns
        if self.actions:
            data['Actions'] = self.actions

        # TEMPORARY
        if not self.table_class:
            data['Objects'] = self.get_queryset()

        data['CreateResource'] = self.get_resource(DashboardCreateView.resource_type)
        data['DetailResource'] = self.get_resource(DashboardDetailView.resource_type)
        data['UpdateResource'] = self.get_resource(DashboardUpdateView.resource_type)
        data['DeleteResource'] = self.get_resource(DashboardDeleteView.resource_type)
        data['ExportResource'] = self.get_resource('export')
        data['TemplateResource'] = self.get_resource('template')
        data['ButtonDetail'] = self.show_option_detail
        data['ButtonUpdate'] = self.show_option_update
        data['ButtonDelete'] = self.show_option_delete

        return data

    def get(self, request, *args, **kwargs):

        table = None
        filter = None
        # filter_helper = None
        template_name = self.template_name

        self.extra_context = self.get_extra_context()

        qs = self.get_queryset()

        if self.filter_class:
            filter = self.filter_class(request.GET, queryset=qs)
            if self.filter_form_helper is not None:
                filter.helper = self.filter_form_helper

        # if self.filter_class:
        #     filter = self.filter_class(request.GET, queryset=qs)

        # self.table_class = self.get_table_class()

        if self.table_class:

            template_name = self.template_name_table

            if filter:
                qs = filter.qs
            table = self.get_table_class(qs)

            if self.lazy_pagination:
                RequestConfig(
                    request, paginate={'per_page': self.per_page, "paginator_class": LazyPaginator}
                ).configure(table)
            else:
                RequestConfig(request, paginate={'per_page': self.per_page}).configure(table)

        check_dashboard_permission(self.request, self.get_resource())
        export_format = request.GET.get('_export', None)

        if TableExport.is_valid_format(export_format):
            exporter = TableExport(
                export_format, table, exclude_columns=['id', 'delete'] + self.table_class.Meta.exclude_from_export)

            return exporter.response(
                f'export-{text.slugify(self.header)}-'
                f'{timezone.localtime(timezone.now()).strftime("%Y%m%d%H%M")}.{export_format}'
            )

        export_template = request.GET.get('_template', None)

        if export_template == 'export':
            exporter = TableExport('xls', table, exclude_columns=self.table_class.Meta.exclude_from_template)
            return exporter.response(
                f'template-{text.slugify(self.header)}-{timezone.localtime(timezone.now()).strftime("%Y%m%d%H%M")}.xls')

        context = self.get_render_context()

        if filter:
            rows = filter.qs.count()
        else:
            rows = qs.count()

        context.update({'list_data': context, 'table': table, 'filter': filter, 'rows':rows})

        if self.send_filtered_qs_to_template:
            context['qs'] = filter.qs

        return render(request, template_name, context)

    def delete_custom_validation(self, ids):
        pass

    def _delete_by_ids(self, ids):
        if ids:
            try:
                check_dashboard_permission(self.request, self.get_resource(DashboardDeleteView.resource_type))
                if not self.allow_delete_by_template:
                    raise PermissionDenied()
                self.delete_custom_validation(ids)
                self.get_queryset().filter(pk__in=ids).delete()
            except ProtectedError:
                raise Exception('Cannot delete some objects because they are related to others.')
            except PermissionDenied:
                raise Exception('You are not allowed to delete items.')
            except Exception as e:
                raise Exception(e)
        pass

    def _get_template_mapping(self):

        template_mapping = {}

        for k, v in self.table_class.base_columns.items():

            if not v.exclude_from_export \
                    and v.verbose_name is not None and \
                    k not in self.table_class.Meta.exclude_from_template and \
                    k not in self.table_class.Meta.ignore_on_template_import \
                    and v.verbose_name not in ['ID', 'DELETE'] \
                    and v.verbose_name not in self.custom_template_key.keys():
                accessor = A(k).get_field(self.object)

                template_mapping[v.verbose_name] = {
                    'key': k,
                    'attname': accessor.name,
                    'is_relation': accessor.is_relation,
                    'related_model': accessor.related_model,
                    'is_date_field': isinstance(accessor, DateField),
                    'is_date_time_field': isinstance(accessor, DateTimeField),
                    'fk': accessor.many_to_one if accessor.is_relation else False,
                    'm2m': accessor.many_to_many if accessor.is_relation else False,
                    'choices': accessor.choices,
                    'choices_array': False  # to deal with ArrayField
                }

        template_mapping.update(self.custom_template_key)

        return template_mapping

    def get_columns_to_ignore_on_template(self):
        return [v.verbose_name for k, v in self.table_class.base_columns.items() if
                             k in self.table_class.Meta.ignore_on_template_import and k not in self.table_class.Meta.exclude_from_template]

    def _format_datetime_df(self, df):
        date_time_columns = [k for k, v in self._get_template_mapping().items() if
                             v['is_date_field'] or v['is_date_time_field']]
        for column in date_time_columns:
            df[column] = pd.to_datetime(df[column])

        return df

    def _format_template_df(self, df):

        template_mapping = self._get_template_mapping()

        df = df.copy()

        if df.empty:
            return df

        for column, data in template_mapping.items():

            if data['is_relation'] and data['fk']: # FK
                df[column] = df[column].apply(
                    lambda s: data['related_model']._filter_by_str(s.strip()) if s != '-' else None)
            elif data['is_relation'] and data['m2m']: #M2M
                df[column] = df[column].apply(lambda s: [data['related_model']._filter_by_str(i.strip()) for i in
                                                         s.split(',')] if s != '-' else [])
            elif not data['is_relation'] and len(data['choices']) > 0:  # choices
                choices = {v: k for k, v in dict(data['choices']).items()}
                try:
                    if data['choices_array']:
                            df[column] = df[column].apply(
                                lambda s: [choices[i.strip()] for i in s.split(',')] if s != '-' else [])
                    else:
                        df[column] = df[column].apply(lambda s: choices[s] if s != '-' else None)
                except:
                    raise ValueError(f"Some choice for column {column} not found.")
            elif data['is_date_field'] or data['is_date_time_field'] and not df.empty:
                try:
                    df[column] = pd.to_datetime(df[column])
                except:
                    raise ValueError(f"Invalid date format for column {column}.")

            df[column].replace('-', '', inplace=True)
            df[column].fillna('', inplace=True)

            df.rename(columns={column: data['attname']}, inplace=True)

        df.rename(columns={'ID': 'id'}, inplace=True)

        df.drop(columns=['DELETE'], errors='ignore', inplace=True)

        df.drop(columns=self.get_columns_to_ignore_on_template(), errors='ignore', inplace=True)

        return df

    def set_extra_include(self, obj):
        return obj

    def _insert_from_dict(self, records):

        if records:

            try:
                check_dashboard_permission(self.request, self.get_resource(DashboardCreateView.resource_type))
                if not self.allow_insert_by_template:
                    raise PermissionDenied()
            except PermissionDenied:
                raise Exception('You are not allowed to insert items.')

            try:
                for record in records:

                    # remove id
                    record.pop('id', None)

                    form_obj = self.form_template_class(record)

                    if form_obj.is_valid():
                        obj = form_obj.save(commit=False)
                        if hasattr(self.object, 'created_by') and 'created_by' not in record:
                            obj.created_by = self.request.user
                        if hasattr(self.object, 'modified_by') and 'modified_by' not in record:
                            obj.modified_by = self.request.user
                        obj = self.set_extra_include(obj)
                        obj.save()
                        form_obj.save_m2m()
                    else:
                        for k, v in form_obj.errors.items():
                            for error in v:
                                raise Exception(error)
            except Exception as e:
                raise Exception(e)

    def _update_from_dict(self, records):

        if records:

            try:
                check_dashboard_permission(self.request, self.get_resource(DashboardUpdateView.resource_type))
            except PermissionDenied:
                raise Exception('You are not allowed to update items.')

            try:
                for record in records:

                    obj_qs = self.get_queryset().filter(pk=record['id'])

                    if obj_qs.exists():
                        instance = obj_qs.first()

                        if hasattr(self.object, 'modified_by') and 'modified_by' not in record:
                            record['modified_by'] = self.request.user

                        form_obj = self.form_template_class(record, instance=instance)

                        if form_obj.is_valid():
                            form_obj.save()
                        else:
                            for k,v in form_obj.errors.items():
                                for error in v:
                                    raise Exception(error)

            except Exception as e:
                raise Exception(e)

    def post(self, request, *args, **kwargs):

        # to deal with actions

        action = request.POST.get('action', None)
        ids = json.loads(request.POST.get('ids', '[]'))
        action_item = self.actions.get(action, None)
        template = request.POST.get('_template', None)
        file = request.FILES.get('file', None)

        if template == 'import' and file:
            template_df = pd.read_excel(file, dtype=self.custom_template_dtype)
            table_columns = [v.verbose_name for k, v in self.table_class.base_columns.items() if
                             not v.exclude_from_export and v.verbose_name is not None and k not in self.table_class.Meta.exclude_from_template]
            if sorted(template_df.columns.tolist()) == sorted(table_columns):

                with transaction.atomic():
                    try:
                        sid = transaction.savepoint()
                        # get objects to delete with ID
                        items_to_delete = template_df.loc[template_df['DELETE'] == True].dropna(axis=0, subset=['ID'])[
                            'ID'].astype(int).tolist()
                        items_to_update_initial = template_df.loc[template_df['DELETE'] == False].dropna(axis=0,
                                                                                                         subset=['ID'])
                        items_to_update_initial['ID'] = items_to_update_initial['ID'].astype(int)
                        items_to_update_initial.fillna(value='-', inplace=True)
                        items_to_update_initial_ids = items_to_update_initial['ID'].tolist()
                        filter_qs = self.get_queryset().filter(pk__in=items_to_update_initial_ids)
                        ids_not_allowed = list(set(items_to_update_initial_ids) - set(filter_qs.values_list('pk', flat=True)))
                        items_to_update_initial = items_to_update_initial[~items_to_update_initial['ID'].isin(ids_not_allowed)]
                        items_to_update_initial_table = self.get_table_class(filter_qs)
                        items_to_update_initial_table = TableExport('xls', items_to_update_initial_table,
                                                                    exclude_columns=self.table_class.Meta.exclude_from_template)
                        items_to_update_initial_df_original = items_to_update_initial_table.dataset.df
                        items_to_update_initial_df_original.fillna(value='-', inplace=True)

                        items_to_update_initial = self._format_datetime_df(items_to_update_initial)
                        items_to_update_initial_df_original = self._format_datetime_df(items_to_update_initial_df_original)

                        items_to_update_initial.drop(columns=self.get_columns_to_ignore_on_template(), inplace=True)
                        items_to_update_initial_df_original.drop(columns=self.get_columns_to_ignore_on_template(), inplace=True)

                        items_to_update = pd.concat(
                            [items_to_update_initial.astype(str), items_to_update_initial_df_original.astype(str),
                             items_to_update_initial_df_original.astype(str)]).drop_duplicates(keep=False)

                        items_to_insert = template_df.loc[template_df.ID.isnull()].fillna(value='-')
                        dict_to_insert = self._format_template_df(items_to_insert).to_dict(orient='records')

                        dict_to_update = self._format_template_df(items_to_update).to_dict(orient='records')

                        self._insert_from_dict(dict_to_insert)
                        self._update_from_dict(dict_to_update)
                        self._delete_by_ids(items_to_delete)
                        transaction.savepoint_commit(sid)

                        messages.success(request, 'Records updated successfully.')

                        # get objects with ID and compare to update
                        # insert new objects
                    except Exception as e:
                        transaction.savepoint_rollback(sid)
                        messages.error(request, 'It was not possible to process template. Check messages and try again.')
                        messages.warning(request, e)
            else:
                messages.warning(request, 'Import and export template columns must match.')
        elif action_item and self.object:

            use_filter_as_qs = action_item.get('use_filter_as_qs', False)
            if use_filter_as_qs:
                ids = list(self.filter_class(request.GET, queryset=self.get_queryset()).qs.values_list('id', flat=True))

            if len(ids) > 0:
                try:
                    is_return_type = action_item.get('return_type', False)
                    method_to_call = getattr(self.object, action_item['function'])
                    if method_to_call:

                        if is_return_type:
                            return method_to_call(request, ids)
                        else:
                            level, message = method_to_call(request, ids)
                            messages.add_message(request, level, message)
                except Warning as w:
                    messages.warning(request, w)
                except Exception as e:
                    messages.error(request, e)
            else:
                messages.warning(request, 'Select at least one item.')

        else:
            messages.error(request, 'Invalid action')

        url_encode = request.GET.urlencode()

        if url_encode != '':
            return redirect(request.path_info + '?' + request.GET.urlencode())

        return redirect(request.path_info)


class DashboardDetailView(DashboardBaseView):

    resource_type = 'detail'

    query_string = ''

    rows_before = {}
    rows_based_on_form = None
    rows_after = {}
    rows_set = {}

    show_button_back = True
    show_button_update = True
    show_button_delete = True

    template_name = 'dashboard/detail.html'

    validate_owner = False

    @abstractmethod
    def get_queryset(self):
        return None

    def get_context(self):

        data = {}

        if self.rows_based_on_form:
            data['RowsBasedOnForm'] = self.rows_based_on_form
        if self.rows_before:
            data['RowsBefore'] = self.rows_before
        if self.rows_after:
            data['RowsAfter'] = self.rows_after
        if self.rows_set:
            data['RowsSet'] = self.rows_set
        if self.object:
            data['Object'] = self.object
        data['ButtonBack'] = self.show_button_back
        data['ButtonUpdate'] = self.show_button_update
        data['ButtonDelete'] = self.show_button_delete

        data['detail_data'] = data

        return data

    def set_object(self, request, pk):

        if not pk:
            raise ValidationError('Needs pk')

        if self.get_queryset():
            obj = get_object_or_404(self.get_queryset(), pk=pk)
        else:
            obj = get_object_or_404(self.object, pk=pk)

        if self.validate_owner and obj.owner != request.user:
            raise PermissionDenied()

        self.object = obj

    def _render_dashboard(self, request, pk=None):

        self.query_string = request.META['QUERY_STRING']

        data_dict = self.get_render_context()

        if not self.query_string:
            return render(request, self.template_name, data_dict)

        data_dict['query_string'] = self.query_string

        return render(request, self.template_name, data_dict)

    def get(self, request, pk, *args, **kwargs):

        check_dashboard_permission(self.request, self.get_resource())

        self.set_object(request, pk)

        return self._render_dashboard(request, pk)


class DashboardCreateView(DashboardBaseView):

    resource_type = 'create'

    form_class = None

    show_button_back = True
    show_button_save_add = True
    show_button_save_continue = False

    owner_include = False

    success_message = ''
    presave_message = ''
    success_redirect = ''
    query_string = ''

    initial = {}

    inlines = []
    inlines_formsets = []

    form_helper = FormSetHelper

    template_name = 'dashboard/form.html'

    def set_extra_include(self, obj):
        return obj

    def set_inlines_formsets(self, request=None, obj=None):

        formsets = []

        for fs in self.inlines:

            formset = inlineformset_factory(
                self.object, fs['model'], form=fs['form'], extra=1, fk_name=fs.get('fk_name', None))

            if request:

                formsets.append(
                    (
                        fs,
                        formset(
                            request.POST or None,
                            request.FILES or None,
                            instance=obj,
                            prefix=fs['model']._meta.model_name
                        ),
                        fs['helper'],
                        {'sortable': fs.get('sortable', False)}
                    )
                )

            else:
                formsets.append(
                    (
                        fs,
                        formset(
                            prefix=fs['model']._meta.model_name
                        ),
                        fs['helper'],
                        {'sortable': fs.get('sortable', False)}
                    )
                )

        self.inlines_formsets = formsets

    def get_context(self):

        data = {}

        if self.form_class:
            data['Form'] = self.form_class
        if self.form_helper:
            data['FormHelper'] = self.form_helper
        data['ButtonBack'] = self.show_button_back
        data['ButtonSaveAdd'] = self.show_button_save_add
        data['ButtonSaveContinue'] = self.show_button_save_continue
        if self.inlines:
            if not self.inlines_formsets:
                self.set_inlines_formsets()
            data['Inlines'] = self.inlines_formsets
        if self.show_button_save_add:
            data['ButtonSaveAddUrl'] = self.request.path

        return {'form_data': data}

    def get(self, request, parent=None, *args, **kwargs):

        check_dashboard_permission(self.request, self.get_resource())

        if self.presave_message:
            messages.info(request, self.presave_message)

        if kwargs:
            self.form_class = self.get_form(request, initial=kwargs)

        self.query_string = request.META['QUERY_STRING']

        data_dict = self.get_render_context()

        if not self.query_string:
            return render(request, self.template_name, data_dict)

        data_dict['query_string'] = self.query_string

        return render(request, self.template_name, data_dict)

    def get_form(self, request, initial=None, *args, **kwargs):

        self.form_class = self.form_class(request.POST or None, request.FILES or None, initial=initial)

        return self.form_class

    def post(self, request, parent=None, *args, **kwargs):

        check_dashboard_permission(self.request, self.get_resource())

        form = self.get_form(request)
        self.set_inlines_formsets(request)
        self.query_string = request.META['QUERY_STRING']

        btn_save_add = request.POST.get('btn_save_add', None)
        btn_save_continue = request.POST.get('btn_save_continue', None)

        if form.is_valid():

            obj = form.save(commit=False)
            self.set_inlines_formsets(request, obj)
            formsets = [inline[1] for inline in self.inlines_formsets]

            if all_valid(formsets):

                if self.owner_include:
                    obj.created_by = request.user
                    obj.modified_by = request.user

                obj = self.set_extra_include(obj)

                try:

                    with transaction.atomic():

                        obj.full_clean()
                        obj.save()
                        form.save_m2m()

                        for inlines_formset in self.inlines_formsets:
                            formset = inlines_formset[1]
                            inline_objs = formset.save(commit=False)

                            for inline_obj in inline_objs:
                                if inlines_formset[0].get('owner_include', False):
                                    inline_obj.modified_by = request.user
                                if not inline_obj.pk and inlines_formset[0].get('owner_include', False):
                                    inline_obj.created_by = request.user

                                inline_obj.save()

                            formset.save_m2m()

                        messages.success(request, self.success_message)

                        if btn_save_add:
                            return redirect(btn_save_add)

                        if btn_save_continue:
                            return redirect(obj.get_update_url())

                        if self.query_string:
                            return redirect(reverse(self.success_redirect) + '?' + self.query_string)

                        return redirect(self.success_redirect)

                except Warning as w:
                    messages.warning(request, w)
                except Exception as e:
                    has_messages = hasattr(e, 'messages')
                    if has_messages:
                        for message in e.messages:
                            messages.error(request, message)
                    else:
                        messages.error(request, e)
        else:
            messages.warning(request, 'Please check the errors below.')

        return render(request, self.template_name, self.get_render_context())


class DashboardUpdateView(DashboardBaseView):

    resource_type = 'update'

    form_class = None
    show_button_back = True

    success_message = ''
    success_redirect = ''
    query_string = ''

    validate_owner = False
    owner_include = False

    form_helper = FormSetHelper

    template_name = 'dashboard/form.html'

    inlines = []
    inlines_formsets = []

    @abstractmethod
    def get_queryset(self):
        return None

    def set_extra_include(self, obj):
        return obj

    def set_initial_form(self):
        return None

    def get_form(self):
        return self.form_class

    def set_inlines_formsets(self, request=None):

        formsets = []

        for fs in self.inlines:

            formset = inlineformset_factory(
                self.object.__class__, fs['model'], form=fs['form'], extra=1, fk_name=fs.get('fk_name', None)
            )

            if request:
                formset_instance = formset(
                    request.POST or None,
                    request.FILES or None,
                    queryset=fs.get('queryset', fs['model'].objects.all()),
                    instance=self.object,
                    prefix=fs['model']._meta.model_name
                )
            else:
                formset_instance = formset(
                    queryset=fs.get('queryset', fs['model'].objects.all()),
                    instance=self.object,
                    prefix=fs['model']._meta.model_name
                )

            formsets.append((fs, formset_instance, fs['helper'], {'sortable': fs.get('sortable', False)}))

        self.inlines_formsets = formsets

    def get_context(self):

        data = {}

        if self.form_class:
            data['Form'] = self.form_class
        if self.object:
            data['Object'] = self.object
        if self.form_helper:
            data['FormHelper'] = self.form_helper
        if self.inlines:
            if not self.inlines_formsets:
                self.set_inlines_formsets()
            data['Inlines'] = self.inlines_formsets
        data['ButtonBack'] = self.show_button_back

        return data

    def set_form(self, request, *args, **kwargs):

        self.form_class = self.get_form()(request.POST or None, request.FILES or None, instance=self.object,
                                          initial=self.set_initial_form())

    def set_object(self, request, pk):
        if not pk:
            raise ValidationError('Needs pk')

        if self.get_queryset():
            obj = get_object_or_404(self.get_queryset(), pk=pk)
        else:
            obj = get_object_or_404(self.object, pk=pk)

        if self.validate_owner and obj.owner != request.user:
            raise PermissionDenied()

        self.object = obj

    def get(self, request, pk=None, *args, **kwargs):

        check_dashboard_permission(self.request, self.get_resource())

        self.set_object(request, pk)
        self.set_form(request)

        context = self.get_render_context()
        context.update({'form_data': context })

        return render(request, self.template_name, context)

    def post(self, request, pk=None, *args, **kwargs):

        check_dashboard_permission(self.request, self.get_resource())

        self.set_object(request, pk)
        self.set_form(request)
        self.set_inlines_formsets(request)
        self.query_string = request.META['QUERY_STRING']

        form = self.form_class

        formsets = [inline[1] for inline in self.inlines_formsets]

        if form.is_valid() and all_valid(formsets):

            obj = form.save(commit=False)

            try:

                with transaction.atomic():

                    if self.owner_include:
                        obj.modified_by = request.user

                    obj = self.set_extra_include(obj)

                    obj.full_clean()
                    obj.save()
                    form.save_m2m()

                    for inlines_formset in self.inlines_formsets:
                        formset = inlines_formset[1]
                        inline_objs = formset.save(commit=False)

                        for inline_obj in inline_objs:
                            if inlines_formset[0].get('owner_include', False):
                                inline_obj.modified_by = request.user
                            if not inline_obj.pk and inlines_formset[0].get('owner_include', False):
                                inline_obj.created_by = request.user

                            inline_obj.save()

                        formset.save_m2m()

                        for deleted_object in formset.deleted_objects:
                            deleted_object.delete()

                    messages.success(request, self.success_message)

                    if self.query_string:
                        return redirect(reverse(self.success_redirect) + '?' + self.query_string)

                    return redirect(self.success_redirect)

            except ProtectedError:
                messages.warning(request, "Cannot delete some objects because they are related to others.")
            except Warning as w:
                messages.warning(request, w)
            except Exception as e:
                has_messages = hasattr(e, 'messages')
                if has_messages:
                    for message in e.messages:
                        messages.error(request, message)
                else:
                    messages.error(request, e)
        else:
            messages.warning(request, 'Please check the errors below.')

        return render(request, self.template_name, {'form_data': self.get_render_context()})


class DashboardDeleteView(DashboardBaseView):

    resource_type = 'delete'

    success_message = ''
    success_redirect = ''
    query_string = ''

    validate_owner = False

    def set_object(self, request, pk):

        if not pk:
            raise ValidationError('Needs pk')

        if self.get_queryset():
            obj = get_object_or_404(self.get_queryset(), pk=pk)
        else:
            obj = get_object_or_404(self.object, pk=pk)

        if self.validate_owner and obj.created_by != request.user:
            raise PermissionDenied()

        self.object = obj

    def custom_validation(self):
        pass

    def post(self, request, pk=None, *args, **kwargs):

        check_dashboard_permission(self.request, self.get_resource())

        self.set_object(request, pk)
        self.query_string = request.META['QUERY_STRING']

        try:
            self.custom_validation()
            self.object.delete()
            messages.success(request, self.success_message)
        except ProtectedError:
            messages.warning(request, "Cannot delete some objects because they are related to others.")
        except Warning as w:
            messages.warning(request, w)
        except Exception as e:
            has_messages = hasattr(e, 'messages')
            if has_messages:
                for message in e.messages:
                    messages.error(request, message)
            else:
                messages.error(request, e)

        if self.query_string:
            return redirect(reverse(self.success_redirect) + '?' + self.query_string)

        return redirect(self.success_redirect)


class DashboardReportView(DashboardBaseView):

    resource_type = 'report'

    form_class = None

    show_button_back = True
    allow_export_xls = True
    allow_export_screen = True

    data = {}
    presave_message = ''

    form_helper = FormSetHelper

    form_template_name = 'dashboard/form_report.html'
    list_template_name = 'dashboard/list_report.html'

    @abstractmethod
    def process_data(self):

        # dict self.data available
        # example of formatters: formatters = {'created_on': lambda x: '<b>' + str(x) + '</b>'}

        df = pd.DataFrame()
        formatters = {}
        show_index = False

        return df, formatters, show_index

    def get_context(self):

        data = {}

        if self.form_class:
            data['Form'] = self.form_class
        if self.form_helper:
            data['FormHelper'] = self.form_helper
        data['ButtonBack'] = self.show_button_back
        data['AllowExportXLS'] = self.allow_export_xls
        data['AllowExportScreen'] = self.allow_export_screen

        return {'form_data': data}

    def get(self, request, parent=None, *args, **kwargs):

        check_dashboard_permission(self.request, self.get_resource())

        if self.presave_message:
            messages.info(request, self.presave_message)

        if kwargs:
            self.form_class = self.get_form(request, initial=kwargs)

        return render(request, self.form_template_name, self.get_render_context())

    def get_form(self, request, initial=None, *args, **kwargs):

        self.form_class = self.form_class(request.POST or None, request.FILES or None, initial=initial)

        return self.form_class

    def post(self, request, *args, **kwargs):

        check_dashboard_permission(self.request, self.get_resource())

        form = self.get_form(request)

        if form.is_valid():

            self.data = form.cleaned_data

            report_data = self.process_data()

            if request.POST.get('btn_submit', None) == 'export':
                with BytesIO() as b:
                    # Use the StringIO object as the filehandle.
                    writer = pd.ExcelWriter(b, engine='xlsxwriter')
                    report_data[0].to_excel(writer, sheet_name=text.slugify(self.header[:30]))
                    writer.save()
                    writer.close()
                    b.seek(0)
                    response = HttpResponse(b, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                    filename = f'report-{text.slugify(self.header)}-{timezone.localtime(timezone.now()).strftime("%Y%m%d%H%M")}.xlsx'
                    response['Content-Disposition'] = 'attachment; filename=%s' % filename

                    return response

                # return to excel

            context = self.get_render_context()
            context.update({'list_data': context, 'report_data': report_data})

            return render(request, self.list_template_name, context)

        else:
            messages.warning(request, 'Please check the errors below.')

        return render(request, self.form_template_name, self.get_render_context())


class DashboardSmartReportView(DashboardBaseView):

    resource_type = 'smart_report'

    form_class = None

    show_button_back = True
    allow_export_xls = True
    allow_export_screen = True

    data = {}
    presave_message = ''

    form_helper = FormSetHelper

    form_template_name = 'dashboard/form_report.html'
    list_template_name = 'dashboard/smart_report.html'

    process_data_without_form = False
    process_data_with_custom_filter = False

    _charts = []
    _tables = []

    def _save_chart(self, data, layout=None, title=None, icon=None):
        if not layout:
            layout = go.Layout(title=title)

        figure = go.Figure(data=data, layout=layout)
        div_plot = opy.plot(figure, include_plotlyjs=False, output_type='div', auto_open=False,
                            config={'displaylogo': False, 'responsive': True})

        self._charts.append({
            'chart': div_plot,
            'title': title,
            'icon': icon
        })

    def _save_table(self, df, title, icon=None, formatters={}, show_index=False, custom_response=None, show_on=('xls', 'screen')):

        self._tables.append({
            'df': df,
            'title': title,
            'icon': icon,
            'formatters': formatters,
            'show_index': show_index,
            'custom_response': custom_response,
            'show_on': show_on
        })

    @abstractmethod
    def process_data(self):

        # self._save_table(pd.DataFrame(), title="Table 1", icon='fa-users')
        #
        # graph1_df = pd.DataFrame(pd.np.array([['Yes', 60], ['No', 40]]), columns=['labels', 'values'])
        # graph1_data = go.Pie(labels=graph1_df['labels'].values.tolist(), values=graph1_df['values'].values.tolist())
        # self._save_chart([graph1_data], title='Graph 1 Title', icon='fa-gears')

        return self._tables, self._charts

    def _render_dashboard(self, request, table_data, chart_data, force_export=False):

        if request.POST.get('btn_submit', None) == 'export' or force_export and self.allow_export_xls:

            with BytesIO() as b:
                # Use the StringIO object as the filehandle.
                writer = pd.ExcelWriter(b, engine='xlsxwriter')
                for table in table_data:
                    if 'xls' in table['show_on']:
                        table['df'].to_excel(writer,
                                             sheet_name=text.slugify(table['title'][:30]),
                                             index=table['show_index']
                        )

                writer.save()
                writer.close()
                b.seek(0)

                response = HttpResponse(
                    b, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                )
                filename = f'report-{text.slugify(self.header)}-' \
                           f'{timezone.localtime(timezone.now()).strftime("%Y%m%d%H%M")}.xlsx'

                response['Content-Disposition'] = 'attachment; filename=%s' % filename

                return response

            # return to excel
        elif self.allow_export_screen:

            context = self.get_render_context()
            table_data = [table for table in table_data if 'screen' in table['show_on']]

            context.update({'list_data': self.get_render_context(), 'table_data': table_data, 'chart_data': chart_data})

            return render(request, self.list_template_name, context)

        else:

            return HttpResponseForbidden()

    def get_context(self):

        data = {}

        if self.form_class:
            data['Form'] = self.form_class
        if self.form_helper:
            data['FormHelper'] = self.form_helper
        data['ButtonBack'] = self.show_button_back
        data['AllowExportXLS'] = self.allow_export_xls
        data['AllowExportScreen'] = self.allow_export_screen

        return {'form_data': data}

    def get(self, request, parent=None, *args, **kwargs):

        check_dashboard_permission(self.request, self.get_resource())

        if self.process_data_without_form:
            table_data, chart_data = self.process_data()

            return self._render_dashboard(request, table_data, chart_data)

        if self.presave_message:
            messages.info(request, self.presave_message)

        if kwargs:
            self.form_class = self.get_form(request, initial=kwargs)

        return render(request, self.form_template_name, self.get_render_context())

    def get_form(self, request, initial=None, *args, **kwargs):

        self.form_class = self.form_class(request.POST or None, request.FILES or None, initial=initial)

        return self.form_class

    def post(self, request, *args, **kwargs):

        check_dashboard_permission(self.request, self.get_resource())

        filter = request.POST.get('filter', False)
        filter_export = request.POST.get('filter_export', False)

        if self.process_data_with_custom_filter and filter:
            table_data, chart_data = self.process_data(filter)

            return self._render_dashboard(request, table_data, chart_data, filter_export)

        form = self.get_form(request)

        if form.is_valid():

            self.data = form.cleaned_data

            table_data, chart_data = self.process_data()

            return self._render_dashboard(request, table_data, chart_data)

        else:
            messages.warning(request, 'Please check the errors below.')

        return render(request, self.form_template_name, self.get_render_context())


class DashboardChartView(DashboardBaseView):

    resource_type = 'chart'

    presend_message = ''

    form_class = None

    show_button_back = True
    allow_export_xls = True
    allow_export_screen = True

    data = {}

    form_helper = FormSetHelper

    form_template_name = 'dashboard/form_report.html'
    data_template = None

    process_data_without_form = False
    dashboard_template = 'dashboard/list_graph.html'

    _charts = {}
    _export_excel = {}

    def _save_chart(self, slug, data, title=None, layout=None, config=None):
        if not layout:
            layout = go.Layout(title=title)

        figure = go.Figure(data=data, layout=layout)
        if not config:
            div_plot = opy.plot(figure, include_plotlyjs=False, output_type='div', auto_open=False,
                                config={'displaylogo': False, 'responsive': True})
        else:
            div_plot = opy.plot(figure, include_plotlyjs=False, output_type='div', auto_open=False,
                                config=config)

        self._charts.update({slug: div_plot})

        return True

    def _save_excel(self, slug, data_frame):
        self._export_excel.update({slug: data_frame})
        return True

    @abstractmethod
    def process_data(self):

        self._charts = {}

        # dict self.data available if process_data_without_form = False

        # graph1_df = pd.DataFrame(pd.np.array([['Yes', 60], ['No', 40]]), columns=['labels', 'values'])
        # graph1_data = go.Pie(labels=graph1_df['labels'].values.tolist(), values=graph1_df['values'].values.tolist())
        # self._save_chart('graph_1', graph1_data, title='Graph 1 Title')

        return self._charts

    def get_context(self):

        data = {}

        if self.form_class:
            data['Form'] = self.form_class
        if self.form_helper:
            data['FormHelper'] = self.form_helper
        if self.data_template:
            data['Data'] = self.data_template

        data['ButtonBack'] = self.show_button_back
        data['AllowExportXLS'] = self.allow_export_xls
        data['AllowExportScreen'] = self.allow_export_screen

        return {'form_data': data, 'list_data': data}

    def _render_dashboard(self, request, chart_data):
        type_request = request.POST.get('btn_submit', None)

        if 'export' in type_request:
            with BytesIO() as b:
                # Use the StringIO object as the filehandle.
                writer = pd.ExcelWriter(b, engine='xlsxwriter')
                for key, value in self._export_excel.items():
                    value.to_excel(writer, sheet_name=text.slugify(key[:30]), index=False)

                writer.save()
                writer.close()
                b.seek(0)

                response = HttpResponse(
                    b, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                )

                filename = f'report-{text.slugify(self.header)}-{timezone.localtime(timezone.now()).strftime("%Y%m%d%H%M")}.xlsx'
                response['Content-Disposition'] = 'attachment; filename=%s' % filename

                return response
                # return to excel

        context = self.get_render_context()
        context.update({'charts': chart_data})

        return render(request, self.dashboard_template, context)

    def get(self, request, *args, **kwargs):

        check_dashboard_permission(self.request, self.get_resource())

        if self.process_data_without_form:
            report_data = self.process_data()
            return self._render_dashboard(request, report_data)

        if self.presend_message:
            messages.info(request, self.presend_message)

        if kwargs:
            self.form_class = self.get_form(request, initial=kwargs)

        return render(request, self.form_template_name, self.get_render_context())

    def get_form(self, request, initial=None, *args, **kwargs):

        self.form_class = self.form_class(request.POST or None, request.FILES or None, initial=initial)

        return self.form_class

    def post(self, request, *args, **kwargs):

        check_dashboard_permission(self.request, self.get_resource())

        form = self.get_form(request)

        try:
            if form.is_valid():
                self.data = form.cleaned_data
                chart_data = self.process_data()
                return self._render_dashboard(request, chart_data)
            else:
                raise ValidationError('Please check the errors below.')
        except ValidationError as e:
            messages.error(request, ', '.join(e.messages))
        except Exception as e:
            messages.error(request, str(e))

        return render(request, self.form_template_name, {'form_data': self.get_context().get('form_data', None)})


class BaseMap(DashboardBaseView):

    page_title = 'Sites Map'
    header = 'Sites Map'

    dashboard_template = 'dashboard/map.html'

    action = None

    use_themes = False
    default_theme = None
    themes = {}

    legend = {}
    cluster_legend = {}
    filter_form_class = None

    bounds = {}

    use_polygons = False
    use_marker_cluster = False
    use_svg_marker = False
    svg_template = 'dashboard/map_svg_icon.html'

    @abstractmethod
    def get_points(self, geometry, theme=None, filter=None):

        return [(
            [-12.34, -56.789],  # Coordinates
            {'id': 'ID', 'name': 'NAME', 'etc': '...'},  # Point Info
            {'color': '#000000', 'text': 'IconText'},  # Icon Info
            1  # Cluster Legend Level
        )], [1,2,3]

    def search_points(self, q, theme=None):
        return [{"loc": [-12.34, -56.789], "title": "Title1"}, {"loc": [12.34, 56.789], "title": "Title2"}]

    def get_svg_icon(self, data={}, theme=None):

        if self.use_themes:
            svg_template = self.themes[theme]['svg_template'] if theme is not None else self.themes[self.default_theme]['svg_template']
        else:
            svg_template = self.svg_template

        return render_to_string(svg_template, context=data)

    def _get_legend(self, theme=None):

        if self.use_themes and theme is not None:
            return self.themes[theme]['legend']

        return self.legend

    def _get_filter_form(self, theme=None):

        form_class = self.filter_form_class

        if self.use_themes and theme is not None:
            form_class = self.themes[theme]['filter_form_class']

        if form_class is not None:
            form = form_class()
            return {
                'form': render_crispy_form(form),
                'form_fields': [key for key in form.fields.keys()]
            }
            # return form_class().as_p()
        return None

    def _get_cluster_legend(self, theme=None):

        if self.use_themes and theme is not None:
            return self.themes[theme]['cluster_legend']

        return self.cluster_legend

    def _get_marker_cluster(self, theme=None):

        if self.use_themes and theme is not None:
            return self.themes[theme]['use_marker_cluster']

        return self.use_marker_cluster

    def _get_polygons(self, theme=None):

        if self.use_themes and theme is not None:
            return self.themes[theme]['use_polygons']

        return self.use_polygons

    def _get_svg_marker(self, theme=None):

        if self.use_themes and theme is not None:
            return self.themes[theme]['use_svg_marker']

        return self.use_svg_marker

    def api_config(self, request):

        theme = request.GET.get('theme', self.default_theme)

        data = {
            'legend': self._get_legend(theme),
            'cluster_legend': self._get_cluster_legend(theme),
            'use_polygons': self._get_polygons(theme),
            'use_marker_cluster': self._get_marker_cluster(theme),
            'use_svg_marker': self._get_svg_marker(theme),
            'filter': self._get_filter_form(theme)
        }

        return JsonResponse(data)

    def api_search(self, request):

        q = request.GET.get('q', None)
        theme = request.GET.get('theme', self.default_theme)

        if not q:
            return JsonResponse({'message': 'Please, inform the Query to proceed!'}, status=400)

        try:
            points = self.search_points(q, theme)

        except Exception as e:
            return JsonResponse(
                {'message': 'Error while trying to get the Physical Sites: {}'.format(str(e))}, status=500
            )

        return JsonResponse({'points': points})

    def _mount_geojson(self, list_data, theme=None):

        results = []

        for data in list_data:

            geo_json = {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': data[0]
                }
            }

            result_json = data[1]

            if self.use_svg_marker or (self.use_themes and theme is not None and self.themes[theme]['use_svg_marker']):
                result_json = dict(result_json, **{'icon': self.get_svg_icon(data[2], theme)})

            if self.use_marker_cluster or (self.use_themes and theme is not None and self.themes[theme]['use_marker_cluster']):
                result_json = dict(result_json, **{'cluster_level': data[3]})

            geo_json['properties'] = result_json

            results.append(geo_json)

        return results

    # @silk_profile(name='Map')
    def api_points(self, request):
        body = json.loads(request.body)
        theme = body.get('theme', None)
        filter = body.get('filter', None)
        self.bounds = body.get('bounds', None)

        if self.use_themes and theme is None:
            theme = self.default_theme

        if not self.bounds:
            return JsonResponse({'message': 'Please inform the bounds!'}, status=400)

        if 'minX' not in self.bounds or 'minY' not in self.bounds or 'maxX' not in self.bounds or 'maxY' not in self.bounds:
            return JsonResponse({'message': 'Please inform all bounds coordinates to proceed!'}, status=400)

        try:
            bbox = (self.bounds['minX'], self.bounds['minY'], self.bounds['maxX'], self.bounds['maxY'])
            geometry = Polygon.from_bbox(bbox)
            points, points_to_remove, polygons, polygons_to_remove = self.get_points(geometry, theme, filter)
            geojson = self._mount_geojson(points, theme)

        except Exception as e:
            return JsonResponse({'message': 'Error while trying to get the Points: {}'.format(str(e))}, status=500)

        return JsonResponse({'points': geojson, 'points_to_remove': points_to_remove, 'polygons': polygons, 'polygons_to_remove': polygons_to_remove})

    def get_point_info(self, point_id, point_coordinates, theme=None):
        return {'code': 'COD', 'name': 'NAME', 'type': 1, 'etc': '...'}

    def api_click(self, request):
        body = json.loads(request.body)
        point_id = body.get('id', None)
        point_coordinates = body.get('coordinates', None)
        theme = body.get('theme', self.default_theme)

        if not point_id and not point_coordinates:
            return JsonResponse({'message': 'Please, inform the Point ID or Coordinates do proceed!'}, status=400)

        try:
            title, info = self.get_point_info(point_id, point_coordinates, theme)

        except ValueError as e:
            return JsonResponse(
                {'message': 'Error while trying to get the Point Information: {}'.format(str(e))}, status=404
            )
        except Exception as e:
            return JsonResponse(
                {'message': 'Error while trying to get the Point Information: {}'.format(str(e))}, status=500
            )

        return JsonResponse({'title': title, 'info': info})

    def api_themes(self, request):

        default_theme = self.default_theme if self.use_themes else None
        themes = { k:{'verbose_name': v['verbose_name']} for k, v in self.themes.items()} if self.use_themes else {}

        return JsonResponse({'default_theme': default_theme, 'themes': themes})

    def _render_map(self, request):

        context = self.get_render_context()

        return render(request, self.dashboard_template, context)

    def get(self, request, *args, **kwargs):

        if self.action == 'config':
            return self.api_config(request)
        elif self.action == 'search':
            return self.api_search(request)
        elif self.action == 'themes':
            return self.api_themes(request)
        elif self.action is None:
            return self._render_map(request)

        return HttpResponseForbidden()

    def post(self, request, *args, **kwargs):

        if self.action == 'points':
            return self.api_points(request)
        elif self.action == 'click':
            return self.api_click(request)

        return HttpResponseForbidden()


class PermissionView(View):
    resource = None
    header = ''

    resource_type = None

    def dispatch(self, request, *args, **kwargs):
        check_dashboard_permission(request, self.get_resource())

        return super().dispatch(request, *args, **kwargs)

    @classmethod
    def get_resource(cls, type_name=None):
        return cls.resource
