from django.urls import reverse

class MenuObject:
    def __init__(self, order, slug, title, url='None', icon='fa-circle-o', visible=True, iframe=False, extra_info=None):
        self.order = order
        self.slug = slug
        self.title = title
        self.url = url
        self.icon = icon
        self.visible = visible
        self.iframe = iframe
        self.extra_info = extra_info or {}


MENUS = [
    MenuObject(order=10, slug='dashboard.*', title="Dashboard", url=reverse("dashboard-index")),
    MenuObject(order=101, slug='dashboard.index', title="Dashboard", url=reverse("dashboard-index"), icon="fa-home"),

    MenuObject(order=20, slug='configuracao.*', title="Configuração", url=reverse("configuracao-index")),
    MenuObject(order=201, slug='configuracao.cadastros.*', title="Cadastros", icon="fa-building"),
    MenuObject(order=2010, slug='configuracao.cadastros.unidades_funcionais', title="Unidades Funcionais", url=reverse("unidade-funcional-list")),
    MenuObject(order=2011, slug='configuracao.cadastros.unidades_escolares', title="Unidades Escolares", url=reverse("unidade-escolar-list")),
    MenuObject(order=2012, slug='configuracao.cadastros.turnos', title="Turnos", url=reverse("turno-list")),
    MenuObject(order=202, slug='configuracao.academico.*', title="Acadêmico", icon="fa-book"),
    MenuObject(order=2020, slug='configuracao.academico.anos_letivos', title="Anos Letivos", url=reverse("ano-letivo-list")),

    MenuObject(order=30, slug='financeiro.*', title="Financeiro", url=reverse("financeiro-index")),
    MenuObject(order=301, slug='financeiro.configuracao.*', title="Configurações", icon="fa-gears"),
    MenuObject(order=3010, slug='financeiro.configuracao.tipos_verbas', title="Tipos de Verbas", url=reverse("tipo-verba-list")),

    MenuObject(order=30, slug='people.manage_users', title="Manage Users", url=reverse("users-profile-list"), icon="fa-user-plus"),
    MenuObject(order=301, slug='people.roles', title="User Roles", url=reverse("users-role-list"), icon="fa-lock"),

    MenuObject(order=40,  slug='profile.*', title="Profile", url=reverse("profile-dashboard"), visible=False),
    MenuObject(order=401, slug='profile.update', title="Change Profile", url=reverse("profile-user-change"), icon='fa-user'),
    MenuObject(order=402, slug='profile.password', title="Change Password", url=reverse("profile-password-change"), icon='fa-key'),
    MenuObject(order=403, slug='profile.password_done', title="Change Password Done", url=reverse("profile-password-done"), icon='fa-key', visible=False),
    MenuObject(order=404, slug='profile.avatar', title="Avatar", url=reverse("profile-avatar-update"), icon='fa-camera'),

    MenuObject(order=50, slug='estudo.*', title="Estudo", url=reverse("estudo-index")),
    MenuObject(order=501, slug='estudo.configuracao.*', title="Configurações", icon="fa-gears"),
    MenuObject(order=5010, slug='estudo.configuracao.informacoes', title="Informações", url=reverse("informacoes-list")),


    MenuObject(order=60, slug='exemplo.*', title="Exemplo", url=reverse("exemplo-index")),
    MenuObject(order=601, slug='exemplo.configuracao.*', title="Configurações", icon="fa-star"),
    MenuObject(order=6010, slug='exemplo.configuracao.exemplo-teste', title="Exemplo Teste", url=reverse("exemplo-teste-list")),

]