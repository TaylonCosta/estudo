from django.db.models import Q, Value
from django.db.models.functions import Concat
from django_filters import Filter, ChoiceFilter

from dashboard.utils import get_date_range, get_datetime_range
from users.models import User


class ListFilter(Filter):
    def filter(self, qs, value):

        if value:
            value_list = value.strip().split('\r\n')
            filtered_items_id = []

            for vl in value_list:
                for item in qs.filter(**{self.field_name + '__icontains': vl}):
                    filtered_items_id.append(item.pk)

            qs = qs.filter(pk__in=filtered_items_id)

        return qs


class UserNameFilter(Filter):
    def filter(self, qs, value):

        if value:

            if self.field_name != '':
                qs = qs.annotate(full_name=Concat('%s__first_name' % (self.field_name), Value(' '), '%s__last_name' % (self.field_name))).filter(full_name__unaccent__icontains=value)
            else:
                qs = qs.annotate(full_name=Concat('first_name', Value(' '), 'last_name')).filter(full_name__unaccent__icontains=value)

        return qs


class MaxResultsFilter(ChoiceFilter):

    def filter(self, qs, value):

        if value:
            qs = qs.filter()[:int(value)]
        return qs


class DateRangeFilter(Filter):

    def filter(self, qs, value):
        if value:

            start, end = get_date_range(value)
            self.lookup_expr = 'range'
            value = (start, end)

        return super().filter(qs, value)


class DateTimeRangeFilter(Filter):

    def filter(self, qs, value):
        if value:

            start, end = get_datetime_range(value)
            self.lookup_expr = 'range'
            value = (start, end)

        return super().filter(qs, value)


class CyberSecurityFilter(Filter):
    def filter(self, qs, value):

        if value:
            start_date, end_date = get_date_range(value)
            qs = qs.filter(profile__cyber_security_date__gte=start_date, profile__cyber_security_date__lte=end_date)
        return qs
