from django.urls import path
from dashboard.views import DashboardIndexView
from users.decorators import register_resource

urlpatterns = [
    path('', register_resource(DashboardIndexView.as_view(page_title='Dashboard', header='Dashboard', columns=[3, 3, 3, 3])), name='dashboard-index'),

]

