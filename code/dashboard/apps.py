from django.apps import AppConfig
from importlib import import_module

from dashboard.config_resources import EXTRA_RESOURCES
from dashboard.permissions import baseResource


class DashboardConfig(AppConfig):
    name = 'dashboard'

    def ready(self):
        baseResource.register_menus()
        baseResource.register_extra_resources(EXTRA_RESOURCES)
