from django_tables2 import tables, columns


class Table(tables.Table):
    select_row = columns.CheckBoxColumn(accessor='pk',
                                        attrs={
                                            "th": {
                                                "style": 'width: 15px'
                                            },
                                            "th__input": {
                                                "onclick": "toggle(this)"
                                            }
                                        },
                                        orderable=False,
                                        exclude_from_export=True)
    actions = columns.TemplateColumn(template_name='dashboard/tags/table_action.html',
                                     attrs={
                                         "th": {
                                             "style": 'width: 75px'
                                         },
                                     },
                                     orderable=False,
                                     exclude_from_export=True)

    id = columns.Column(verbose_name='ID', accessor='pk', visible=False)
    delete = columns.BooleanColumn(verbose_name='DELETE', empty_values=(), visible=False)

    def render_delete(self):
        return False

    class Meta:
        # model = City
        attrs = {
            'class': 'table table-bordered table-hover dataTable no-footer',
            'th': {
                '_ordering': {
                    'orderable': 'sorting',
                    'ascending': 'sorting_asc',
                    'descending': 'sorting_desc'
                }
            }
        }
        sequence = ('select_row', 'id', 'actions', '...', 'delete')
        # exclude = ('select_row', )
        exclude_from_export = []
        exclude_from_template = []
        ignore_on_template_import = []
