from abc import abstractmethod

from django.db import models
from django.urls import reverse

import re


class DashboardBaseModel(models.Model):

    # Automatic
    created_by = models.ForeignKey('users.User', related_name="%(class)s_informer", on_delete=models.PROTECT)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey('users.User', related_name="%(class)s_updater", on_delete=models.PROTECT)
    modified_on = models.DateTimeField(auto_now=True)

    # class AuthManager(models.Manager):
    #     def allowed(self, user):
    #         qs = super().get_queryset().filter()
    #
    #         from libs.permissions.permissions import get_allowed_product_lines, get_allowed_accounts
    #         allowed_accounts = get_allowed_accounts(user, ids=True)
    #         if allowed_accounts:
    #             qs = qs.filter(account__pk__in=allowed_accounts)
    #
    #         allowed_product_lines = get_allowed_product_lines(user, ids=True)
    #         if allowed_product_lines:
    #             qs = qs.filter(product_line__pk__in=allowed_product_lines)
    #
    #         return qs
    #
    # objects = AuthManager()

    class Meta:
        abstract = True

    @classmethod
    @abstractmethod
    def set_filter_by_str(cls, q):

        return {}

    @classmethod
    def _filter_by_str(cls, q, first_match_id=True, raise_when_not_found=True):

        filter = cls.set_filter_by_str(q)

        qs = cls.objects.filter(**filter)

        if first_match_id:
            if qs.exists():
                return qs.first().id
            elif raise_when_not_found:
                raise ValueError(f'{cls._meta.verbose_name} {q} inexistente')
            return None

        return qs

    def get_delete_url(self):
        model_name = re.sub('(?!^)([A-Z]+)', r'-\1', self._meta.object_name).lower()
        return reverse(f"{model_name}-delete", kwargs={'pk': self.pk})

    def get_update_url(self):
        model_name = re.sub('(?!^)([A-Z]+)', r'-\1', self._meta.object_name).lower()
        return reverse(f"{model_name}-update", kwargs={'pk': self.pk})

    def get_absolute_url(self):
        model_name = re.sub('(?!^)([A-Z]+)', r'-\1', self._meta.object_name).lower()
        return reverse(f"{model_name}-detail", kwargs={'pk': self.pk})


class DashboardBaseModelNoUser(models.Model):

    # Automatic
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    def get_delete_url(self):
        model_name = re.sub('(?!^)([A-Z]+)', r'-\1', self._meta.object_name).lower()
        return reverse(f"{model_name}-delete", kwargs={'pk': self.pk})

    def get_update_url(self):
        model_name = re.sub('(?!^)([A-Z]+)', r'-\1', self._meta.object_name).lower()
        return reverse(f"{model_name}-update", kwargs={'pk': self.pk})

    def get_absolute_url(self):
        model_name = re.sub('(?!^)([A-Z]+)', r'-\1', self._meta.object_name).lower()
        return reverse(f"{model_name}-detail", kwargs={'pk': self.pk})

