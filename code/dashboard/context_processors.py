from django.conf import settings


def dashboard_info(request):

    return {
        'SYSTEM_VERSION': settings.VERSION,
        'DEPLOY_ENV': settings.ACTUAL_ENV,
        'STAGING_CODE_VERSION': settings.STAGING_CODE_VERSION,
        'STAGING_DATABASE_VERSION': settings.STAGING_DATABASE_VERSION,

    }
