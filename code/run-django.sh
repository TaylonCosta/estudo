#!/bin/sh

cd /code

until python test_database.py; do
  echo "Database is unavailable - sleeping"
  sleep 1
done

python manage.py migrate
if [ -f IGNORE_COLLECTSTATIC.txt ]; then echo 'Ignoring collectstatic...' ; else python manage.py collectstatic --noinput; fi;

gunicorn proj.wsgi:application --limit-request-line 0 --timeout 600 -w 4 -b :8000