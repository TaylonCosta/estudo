from dal import autocomplete

from estudo.models import Informacoes


class InformacoesAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):

        qs = Informacoes.objects.filter()

        if self.q:
            qs = qs.filter(nome__icontains=self.q)
        return qs
