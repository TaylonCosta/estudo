from django_tables2 import columns
from dashboard import tables as dashboard_table
from estudo.models import Informacoes


class InformacoesTable(dashboard_table.Table):

    class Meta(dashboard_table.Table.Meta):
        model = Informacoes
        fields = ['nome','endereco']
        exclude = ('select_row', )
