from django.contrib.auth.decorators import login_required
from django.urls import path, include
from django.views.generic import TemplateView

from dashboard.views import DashboardIndexView
from estudo.autocomplete import InformacoesAutocomplete
from estudo.views import InformacoesCreate, InformacoesList, InformacoesDelete, InformacoesUpdate, InformacoesDetail
from users.decorators import register_resource

urlpatterns = [
   path('index_informacao/',register_resource(DashboardIndexView),name='estudo-index'),
   path('informacoes/', include([
       path('', register_resource(InformacoesList), name='informacoes-list'),
       path('create/', register_resource(InformacoesCreate), name='informacoes-create'),
       path('detail/<int:pk>', register_resource(InformacoesDetail), name='informacoes-detail'),
       path('update/<int:pk>', register_resource(InformacoesUpdate), name='informacoes-update'),
       path('delete/<int:pk>', register_resource(InformacoesDelete), name='informacoes-delete'),
       path('autocomplete/', login_required(InformacoesAutocomplete.as_view()), name='informacoes-autocomplete'),
   ])),


]
