from django.shortcuts import render
from django.urls import reverse_lazy
from dashboard.views import DashboardListView, DashboardDeleteView, DashboardUpdateView, DashboardCreateView, \
    DashboardDetailView
from estudo.filter import InformacoesFilter
from estudo.forms import InformacoesForm
from estudo.models import Informacoes
from estudo.tables import InformacoesTable


class InformacoesList(DashboardListView):

    page_title = 'Informacoes'
    header = 'Informações list'

    add_button_title = 'New Informações'
    add_button_url = reverse_lazy('informacoes-create')

    table_class = InformacoesTable
    filter_class = InformacoesFilter
    object = Informacoes

    def get_queryset(self):
        qs = Informacoes.objects.filter()
        return qs


class InformacoesDetail(DashboardDetailView):

    page_title = 'Informacoes'
    header = 'Informações detail'
    object = Informacoes

    rows_based_on_form = InformacoesForm

    def get_queryset(self):
        qs = Informacoes.objects.filter()
        return qs


class InformacoesCreate(DashboardCreateView):

    page_title = 'Informacoes'
    header = 'Add Informações'

    form_class = InformacoesForm
    object = Informacoes

    show_button_save_continue = True
    owner_include = True

    success_message = 'Informações created successfully.'

    success_redirect = 'informacoes-list'


class InformacoesUpdate(DashboardUpdateView):

    page_title = 'Informacoes'
    header = 'Edit Informações'

    form_class = InformacoesForm
    object = Informacoes

    success_message = 'Informações updated successfully.'
    success_redirect = 'informacoes-list'

    def get_queryset(self):
        qs = Informacoes.objects.filter()
        return qs


class InformacoesDelete(DashboardDeleteView):

    success_message = 'Informações deleted successfully.'
    success_redirect = 'informacoes-list'

    header = 'Delete Informações'

    object = Informacoes
    validate_owner = False

    def get_queryset(self):
        qs = Informacoes.objects.filter()
        return qs



# Create your views here.
