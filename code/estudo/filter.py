import django_filters
from dal import autocomplete

from estudo.models import Informacoes


class InformacoesFilter(django_filters.FilterSet):

    class Meta:
        model = Informacoes
        fields = {
            'nome': ['iexact'],

        }

