from django.db import models

from dashboard.models import DashboardBaseModel


class Informacoes(DashboardBaseModel):

    nome = models.CharField('Nome',max_length=200, blank=True)
    endereco = models.CharField('Endereco',max_length=200, blank=True)
# Create your models here.
