from dal import autocomplete, forward
from django import forms

from estudo.models import Informacoes


class InformacoesForm(forms.ModelForm):

    class Meta:
        model = Informacoes
        fields = '__all__'
        exclude = ('created_by', 'created_on', 'modified_by', 'modified_on')
