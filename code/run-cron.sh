#!/bin/sh

cd /code

until python test_database.py; do
  echo "Database is unavailable - sleeping"
  sleep 1
done

service cron start

touch /tmp/execute.log
touch /tmp/check.log

cat /etc/cron.d/proj.jobs | crontab -

tail -f /tmp/*.log 2> /dev/null