from django_tables2 import columns
from dashboard import tables as dashboard_table
from exemplo.models import ExemploTeste


class ExemploTesteTable(dashboard_table.Table):

    class Meta(dashboard_table.Table.Meta):
        model = ExemploTeste
        fields = ['teste1']
        exclude = ('status', )
