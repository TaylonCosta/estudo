from django.shortcuts import render

# Create your views here.


from django.urls import reverse_lazy
from dashboard.views import DashboardListView, DashboardDeleteView, DashboardUpdateView, DashboardCreateView, \
    DashboardDetailView
from exemplo.filter import ExemploTesteFilter
from exemplo.forms import ExemploTesteForm
from exemplo.models import ExemploTeste
from exemplo.tables import ExemploTesteTable


class ExemploTesteList(DashboardListView):

    page_title = 'Exemplo Teste'
    header = 'Exemplo teste list'

    add_button_title = 'New Exemplo teste'
    add_button_url = reverse_lazy('exemplo-teste-create')

    table_class = ExemploTesteTable
    filter_class = ExemploTesteFilter
    object = ExemploTeste

    def get_queryset(self):
        qs = ExemploTeste.objects.filter()
        return qs


class ExemploTesteDetail(DashboardDetailView):

    page_title = 'Exemplo Teste'
    header = 'Exemplo teste detail'
    object = ExemploTeste

    rows_based_on_form = ExemploTesteForm

    def get_queryset(self):
        qs = ExemploTeste.objects.filter()
        return qs


class ExemploTesteCreate(DashboardCreateView):

    page_title = 'Exemplo Teste'
    header = 'Add Exemplo teste'

    form_class = ExemploTesteForm
    object = ExemploTeste

    show_button_save_continue = True
    owner_include = True

    success_message = 'Exemplo teste created successfully.'

    success_redirect = 'exemplo-teste-list'


class ExemploTesteUpdate(DashboardUpdateView):

    page_title = 'Exemplo Teste'
    header = 'Edit Exemplo teste'

    form_class = ExemploTesteForm
    object = ExemploTeste

    success_message = 'Exemplo teste updated successfully.'
    success_redirect = 'exemplo-teste-list'

    def get_queryset(self):
        qs = ExemploTeste.objects.filter()
        return qs


class ExemploTesteDelete(DashboardDeleteView):

    success_message = 'Exemplo teste deleted successfully.'
    success_redirect = 'exemplo-teste-list'

    header = 'Delete Exemplo teste'

    object = ExemploTeste
    validate_owner = False

    def get_queryset(self):
        qs = ExemploTeste.objects.filter()
        return qs

