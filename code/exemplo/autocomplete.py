from dal import autocomplete

from exemplo.models import ExemploTeste


class ExemploTesteAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):

        qs = ExemploTeste.objects.filter()

        if self.q:
            qs = qs.filter(teste1__icontains=self.q)
        return qs
