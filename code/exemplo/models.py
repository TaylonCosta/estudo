
from django.db import models

from dashboard.models import DashboardBaseModel


class ExemploTeste(DashboardBaseModel):

    teste1 = models.CharField('Nome',max_length=200, blank=True)
    texto = models.TextField('Texto', blank=True)
    status = models.BooleanField('Status',default=True, blank=True)