from django.contrib.auth.decorators import login_required
from django.urls import path, include
from django.views.generic import TemplateView

from dashboard.views import DashboardIndexView
from exemplo.autocomplete import ExemploTesteAutocomplete
from exemplo.views import ExemploTesteList, ExemploTesteCreate, ExemploTesteDelete, ExemploTesteUpdate, \
    ExemploTesteDetail
from users.decorators import register_resource

urlpatterns = [

path('index_exemploteste/',register_resource(DashboardIndexView),name='exemplo-index'),
    path('exemplo-teste/', include([
        path('', register_resource(ExemploTesteList), name='exemplo-teste-list'),
        path('create/', register_resource(ExemploTesteCreate), name='exemplo-teste-create'),
        path('detail/<int:pk>', register_resource(ExemploTesteDetail), name='exemplo-teste-detail'),
        path('update/<int:pk>', register_resource(ExemploTesteUpdate), name='exemplo-teste-update'),
        path('delete/<int:pk>', register_resource(ExemploTesteDelete), name='exemplo-teste-delete'),
        path('autocomplete/', login_required(ExemploTesteAutocomplete.as_view()), name='exemplo-teste-autocomplete'),
    ])),

]





    