import django_filters
from dal import autocomplete

from exemplo.models import ExemploTeste


class ExemploTesteFilter(django_filters.FilterSet):

    class Meta:
        model = ExemploTeste
        fields = {
            'teste1': ['icontains'],
            
        }
