from dal import autocomplete, forward
from django import forms

from exemplo.models import ExemploTeste


class ExemploTesteForm(forms.ModelForm):

    class Meta:
        model = ExemploTeste
        fields = '__all__'
        exclude = ('created_by', 'created_on', 'modified_by', 'modified_on')
